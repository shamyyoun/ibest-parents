package com.advisoryme.ibest.parents.data.apis.bodies

import com.google.gson.annotations.SerializedName

data class ChangePasswordBody(
        @SerializedName("new_password")
        val newPassword: String = "",
        @SerializedName("old_password")
        val oldPassword: String = ""
)