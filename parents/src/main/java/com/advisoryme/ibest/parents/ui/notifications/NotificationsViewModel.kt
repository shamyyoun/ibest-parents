package com.advisoryme.ibest.parents.ui.notifications

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.parents.data.models.Notification
import com.advisoryme.ibest.parents.ui.base.BaseViewModel
import com.advisoryme.ibest.parents.utils.InjectionUtils

class NotificationsViewModel(app: Application) : BaseViewModel(app) {
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    private val notificationsRepository = InjectionUtils.provideNotificationsRepository(app, activeUserRepository.getUser()!!.token)
    val notificationsEvent = MutableLiveData<List<Notification>>()
    val emptyEvent = MutableLiveData<Boolean>()

    fun fetchNotifications() {
        with(notificationsRepository.getNotifications()) {
            if (isNotEmpty()) {
                notificationsEvent.value = this
            } else {
                emptyEvent.value = true
            }
        }
    }
}