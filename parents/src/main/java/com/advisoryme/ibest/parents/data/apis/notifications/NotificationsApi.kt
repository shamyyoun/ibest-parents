package com.advisoryme.ibest.parents.data.apis.notifications

import com.advisoryme.ibest.parents.data.apis.bodies.UpdateFCMTokenBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface NotificationsApi {
    @POST("fcm_token")
    fun updateFCMToken(@Body body: UpdateFCMTokenBody): Call<Void>
}