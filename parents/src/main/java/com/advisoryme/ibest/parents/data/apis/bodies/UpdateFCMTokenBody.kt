package com.advisoryme.ibest.parents.data.apis.bodies

import com.google.gson.annotations.SerializedName

data class UpdateFCMTokenBody(
        @SerializedName("token")
        val fcmToken: String = ""
)