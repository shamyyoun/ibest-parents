package com.advisoryme.ibest.parents.ui.signup

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.data.models.User
import com.advisoryme.ibest.parents.ui.base.BaseViewModel
import com.advisoryme.ibest.parents.utils.InjectionUtils

class AddPasswordViewModel(app: Application, private val user: User) : BaseViewModel(app) {
    private val updatePasswordRepository = InjectionUtils.provideUpdatePasswordRepository(user.token)
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    val addPasswordEvent = MutableLiveData<Boolean>()

    fun addPassword(newPassword: String) {
        // validate
        if (validateInternetConnection() && validateChangePasswordInputs(newPassword)) {
            triggerProgress()

            updatePasswordRepository.changePassword(newPassword).observeForever {
                triggerProgress()

                // process repo result then trigger the event
                handleRepositoryResult(it) {
                    activeUserRepository.updateUser(user)
                    addPasswordEvent.value = it?.result
                }
            }
        }
    }

    private fun validateChangePasswordInputs(newPassword: String): Boolean {
        return when {
            newPassword.isEmpty() -> {
                triggerError(R.string.enter_password)
                false
            }
            newPassword.length < 6 -> {
                triggerError(R.string.too_short_password)
                false
            }
            else -> true
        }
    }
}