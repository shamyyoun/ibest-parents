package com.advisoryme.ibest.parents.data.apis.updatepassword

import com.advisoryme.ibest.parents.data.apis.bodies.ChangePasswordBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UpdatePasswordApi {
    @POST("change_password")
    fun changePassword(@Body body: ChangePasswordBody): Call<Void>
}