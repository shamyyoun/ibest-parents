package com.advisoryme.ibest.parents.data.apis.updatepassword

import com.advisoryme.ibest.parents.data.apis.interceptors.AuthorizationInterceptor
import com.advisoryme.ibest.parents.data.apis.utils.ApiInjectionUtils
import com.advisoryme.ibest.parents.utils.InjectionUtils
import retrofit2.Call

class UpdatePasswordApiImpl(token: String) {
    private val updatePasswordApi: UpdatePasswordApi

    init {
        val authInterceptor = AuthorizationInterceptor(token)
        val apiManagerImpl = InjectionUtils.provideApiManager(authInterceptor)
        updatePasswordApi = apiManagerImpl.createApi(UpdatePasswordApi::class.java)
    }

    fun changePassword(newPassword: String, oldPassword: String): Call<Void> {
        val body = ApiInjectionUtils.provideChangePasswordBody(newPassword, oldPassword)
        return updatePasswordApi.changePassword(body)
    }
}