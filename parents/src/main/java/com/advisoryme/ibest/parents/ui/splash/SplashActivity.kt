package com.advisoryme.ibest.parents.ui.splash

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.ui.base.BaseActivity
import com.advisoryme.ibest.parents.ui.login.LoginActivity
import com.advisoryme.ibest.parents.ui.main.MainActivity
import com.advisoryme.ibest.parents.ui.signup.SignupActivity
import com.advisoryme.ibest.parents.utils.InjectionUtils

class SplashActivity : BaseActivity<SplashViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // start splash
        viewModel.startSplash()
    }

    override fun getTheViewModel() = InjectionUtils.provideSplashViewModel(this)

    override fun observe() {
        super.observe()

        // user event
        viewModel.hasUserEvent.observe(this, Observer {
            when (it) {
                true -> openMainActivity()
            }
        })
    }

    fun onSignin(v: View) = openLoginActivity()

    fun onSignup(v: View) = openSignupActivity()

    private fun openMainActivity() = startActivity(MainActivity.newIntent(this))

    private fun openLoginActivity() = startActivity(LoginActivity.newIntent(this))

    private fun openSignupActivity() = startActivity(SignupActivity.newIntent(this))

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, SplashActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    or Intent.FLAG_ACTIVITY_NEW_TASK)
            return intent
        }
    }
}
