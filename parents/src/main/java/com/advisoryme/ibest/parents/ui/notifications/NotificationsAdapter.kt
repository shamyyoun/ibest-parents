package com.advisoryme.ibest.parents.ui.notifications

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.data.models.Notification

class NotificationsAdapter(var data: List<Notification>) : RecyclerView.Adapter<NotificationViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return NotificationViewHolder(layoutInflater.inflate(R.layout.item_notification, parent, false))
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        with(data[position]) {
            holder.bind(this)
        }
    }
}