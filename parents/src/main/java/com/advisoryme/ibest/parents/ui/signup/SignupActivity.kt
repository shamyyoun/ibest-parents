package com.advisoryme.ibest.parents.ui.signup

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.ui.base.BaseActivity
import com.advisoryme.ibest.parents.ui.main.MainActivity
import com.advisoryme.ibest.parents.utils.InjectionUtils
import com.advisoryme.ibest.parents.utils.Utils
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : BaseActivity<SignupViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // add done listeners
        etNumber.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signup()
            }
            true
        }
        etCode.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                confirmCode()
            }
            true
        }
    }

    override fun getTheViewModel() = InjectionUtils.provideSignupViewModel(this)

    override fun observe() {
        super.observe()

        // signup event
        viewModel.signupEvent.observe(this, Observer {
            if (it == true) {
                // show msg and show verification code section
                showLongToast(R.string.verification_code_has_been_sent)
                showVerificationCodeSection()
            }
        })

        // confirm code event
        viewModel.confirmCodeEvent.observe(this, Observer {
            if (it != null) {
                // show msg
                showLongToast(R.string.successful_verification)

                // show add password dialog
                AddPasswordDialog(this@SignupActivity, it) {
                    // once password changed, open main activity
                    openMainActivity()
                }.show()
            }
        })
    }

    fun onVerify(v: View) = signup()

    private fun signup() = viewModel.signup(etNumber.text.toString())

    fun onConfirm(v: View) = confirmCode()

    private fun confirmCode() = viewModel.confirmCode(etNumber.text.toString(), Utils.convertToInt(etCode.text.toString()))

    private fun showVerificationCodeSection() {
        groupVerification.visibility = View.VISIBLE
        etCode.requestFocus()
    }

    private fun openMainActivity() = startActivity(MainActivity.newIntent(this))

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SignupActivity::class.java)
        }
    }
}
