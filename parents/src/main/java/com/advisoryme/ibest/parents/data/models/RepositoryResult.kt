package com.advisoryme.ibest.parents.data.models

data class RepositoryResult<T>(val result: T?,
                               val message: String?
)