package com.advisoryme.ibest.parents.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import android.view.animation.AlphaAnimation

/**
 * Created by Shamyyoun on 2/3/2016.
 */
object ViewUtil {
    const val DEFAULT_DURATION = 250

    /**
     * method used to show or hide view in default duration
     */
    fun fadeView(view: View, show: Boolean) {
        fadeView(view, show, DEFAULT_DURATION, View.GONE)
    }

    /**
     * method used to show or hide view
     */
    fun fadeView(view: View, show: Boolean, duration: Int) {
        fadeView(view, show, duration, View.GONE)
    }

    /**
     * method used to show or hide view with INVISIBLE constant in default duration
     */
    fun hideView(view: View, invisibleConstant: Int) {
        fadeView(view, false, DEFAULT_DURATION, invisibleConstant)
    }

    /**
     * method used to show or hide view with INVISIBLE constant
     */
    fun hideView(view: View, duration: Int, invisibleConstant: Int) {
        fadeView(view, false, duration, invisibleConstant)
    }

    /**
     * method used to shows or hides a view with a smooth animation in specific duration
     */
    private fun fadeView(view: View?, show: Boolean, duration: Int, invisibleConstant: Int) {
        view?.animate()?.setDuration(duration.toLong())?.alpha((if (show) 1 else 0).toFloat())?.setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                if (show)
                    view.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animator) {
                if (!show)
                    view.visibility = invisibleConstant
            }
        })
    }

    /**
     * method, used to fade in view with passed duration
     *
     * @param view
     * @param duration
     */
    fun fadeInView(view: View?, duration: Int) {
        if (view != null) {
            val animation = AlphaAnimation(0f, 1f)
            animation.duration = duration.toLong()
            animation.fillAfter = true
            view.visibility = View.VISIBLE
            view.startAnimation(animation)
        }
    }

    /**
     * Apply alpha to a view in code
     *
     * @param view  to apply alpha on it
     * @param alpha value to apply
     */
    fun alpha(view: View, alpha: Float) {
        val animation = AlphaAnimation(alpha, alpha)
        animation.duration = 0
        animation.fillAfter = true
        view.startAnimation(animation)
    }

    /**
     * Apply alpha to a view in code with a duration to the animation
     *
     * @param view     to apply alpha on it
     * @param alpha    value to apply
     * @param duration value for the animation
     */
    fun alpha(view: View, alpha: Float, duration: Long) {
        val animation = AlphaAnimation(view.alpha, alpha)
        animation.duration = duration
        animation.fillAfter = true
        view.startAnimation(animation)
    }

    /**
     * method, used to show first view and hide other views
     *
     * @param viewToShow
     * @param viewsToHide
     */
    fun showOneView(viewToShow: View, vararg viewsToHide: View) {
        viewToShow.visibility = View.VISIBLE
        for (view in viewsToHide) {
            view.visibility = View.GONE
        }
    }
}
