package com.advisoryme.ibest.parents.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Notification(
        @PrimaryKey(autoGenerate = true) var id: Int = 0,
        var date: Long = 0,
        var content: String = ""
)