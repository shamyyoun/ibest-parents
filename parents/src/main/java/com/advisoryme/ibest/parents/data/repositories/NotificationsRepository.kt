package com.advisoryme.ibest.parents.data.repositories

import android.arch.lifecycle.LiveData
import android.content.Context
import com.advisoryme.ibest.parents.data.models.Notification
import com.advisoryme.ibest.parents.data.models.RepositoryResult
import com.advisoryme.ibest.parents.utils.Const
import com.advisoryme.ibest.parents.utils.InjectionUtils

class NotificationsRepository(context: Context, token: String) : BaseRepository() {
    private val notificationsApi = InjectionUtils.provideNotificationsApi(token)
    private val prefsManager = InjectionUtils.providePrefsManager(context)
    private val notificationDao = InjectionUtils.provideNotificationDao(context)

    fun updateFCMToken(fcmToken: String): LiveData<RepositoryResult<Boolean>> {
        val call = notificationsApi.updateFCMToken(fcmToken)
        return handleApiResponse(call)
    }

    fun isFCMTokenUpdated() = prefsManager.load<Boolean>(Const.KEY_FCM_TOKEN_UPDATED, Boolean::class.java)

    fun setFCMTokenUpdated() = prefsManager.save(true, Const.KEY_FCM_TOKEN_UPDATED)

    fun getNotifications() = notificationDao.getLatest()

    fun saveNotification(notification: Notification) = notificationDao.insert(notification)

    fun clearNotifications() = notificationDao.clear()
}