package com.advisoryme.ibest.parents.utils

import android.animation.ObjectAnimator
import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.os.Build
import android.os.Handler
import android.os.SystemClock
import android.util.Property
import android.view.animation.AccelerateDecelerateInterpolator
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import java.lang.Math.*

object MarkerUtils {
    fun animateMarkerToGB(marker: Marker, latLng: LatLng, latLngInterpolator: LatLngInterpolator) {
        val startPosition = marker.position
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val interpolator = AccelerateDecelerateInterpolator()
        val durationInMs = 3000f

        handler.post(object : Runnable {
            internal var elapsed: Long = 0
            internal var t: Float = 0.toFloat()
            internal var v: Float = 0.toFloat()

            override fun run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start
                t = elapsed / durationInMs
                v = interpolator.getInterpolation(t)

                marker.position = latLngInterpolator.interpolate(v, startPosition, latLng)

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                }
            }
        })
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    fun animateMarkerToHC(marker: Marker, finalPosition: LatLng, latLngInterpolator: LatLngInterpolator) {
        val startPosition = marker.position

        val valueAnimator = ValueAnimator()
        valueAnimator.addUpdateListener { animation ->
            val v = animation.animatedFraction
            val newPosition = latLngInterpolator.interpolate(v, startPosition, finalPosition)
            marker.position = newPosition
        }
        valueAnimator.setFloatValues(0f, 1f) // Ignored.
        valueAnimator.duration = 3000
        valueAnimator.start()
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    fun animateMarkerToICS(marker: Marker, finalPosition: LatLng, latLngInterpolator: LatLngInterpolator) {
        val latLngEvaluator = TypeEvaluator<LatLng> { fraction, startValue, endValue -> latLngInterpolator.interpolate(fraction, startValue, endValue) }
        val position = Property.of(Marker::class.java, LatLng::class.java, "position")
        val positionAnimator = ObjectAnimator.ofObject(marker, position, latLngEvaluator, finalPosition)
        positionAnimator.setDuration(1000).start()
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    fun animateMarkerToICSWithBearing(marker: Marker, finalPosition: LatLng, finalRotation: Float, latLngInterpolator: LatLngInterpolator) {
        val latLngEvaluator = TypeEvaluator<LatLng> { fraction, startValue, endValue -> latLngInterpolator.interpolate(fraction, startValue, endValue) }
        val position = Property.of(Marker::class.java, LatLng::class.java, "position")
        val positionAnimator = ObjectAnimator.ofObject(marker, position, latLngEvaluator, finalPosition)
        positionAnimator.setDuration(1000).start()

        val floatEvaluator = TypeEvaluator<Float> { fraction, startValue, endValue ->
            var startValue = startValue
            var endValue = endValue
            if (endValue!! - startValue!! > 180)
                startValue += 360
            else if (endValue - startValue < -180) endValue += 360
            (endValue - startValue) * fraction + startValue
        }
        val rotation = Property.of(Marker::class.java, Float::class.java, "rotation")
        val rotationAnimator = ObjectAnimator.ofObject(marker, rotation, floatEvaluator, finalRotation)
        rotationAnimator.setDuration(1000).start()
    }

    /**
     * LatLngInterpolator class
     */
    interface LatLngInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng

        class Linear : LatLngInterpolator {
            override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                val lng = (b.longitude - a.longitude) * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }

        class LinearFixed : LatLngInterpolator {
            override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                var lngDelta = b.longitude - a.longitude

                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360
                }
                val lng = lngDelta * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }

        class Spherical : LatLngInterpolator {

            /* From github.com/googlemaps/android-maps-utils */
            override fun interpolate(fraction: Float, from: LatLng, to: LatLng): LatLng {
                // http://en.wikipedia.org/wiki/Slerp
                val fromLat = toRadians(from.latitude)
                val fromLng = toRadians(from.longitude)
                val toLat = toRadians(to.latitude)
                val toLng = toRadians(to.longitude)
                val cosFromLat = cos(fromLat)
                val cosToLat = cos(toLat)

                // Computes Spherical interpolation coefficients.
                val angle = computeAngleBetween(fromLat, fromLng, toLat, toLng)
                val sinAngle = sin(angle)
                if (sinAngle < 1E-6) {
                    return from
                }
                val a = sin((1 - fraction) * angle) / sinAngle
                val b = sin(fraction * angle) / sinAngle

                // Converts from polar to vector and interpolate.
                val x = a * cosFromLat * cos(fromLng) + b * cosToLat * cos(toLng)
                val y = a * cosFromLat * sin(fromLng) + b * cosToLat * sin(toLng)
                val z = a * sin(fromLat) + b * sin(toLat)

                // Converts interpolated vector back to polar.
                val lat = atan2(z, sqrt(x * x + y * y))
                val lng = atan2(y, x)
                return LatLng(toDegrees(lat), toDegrees(lng))
            }

            private fun computeAngleBetween(fromLat: Double, fromLng: Double, toLat: Double, toLng: Double): Double {
                // Haversine's formula
                val dLat = fromLat - toLat
                val dLng = fromLng - toLng
                return 2 * asin(sqrt(pow(sin(dLat / 2), 2.0) + cos(fromLat) * cos(toLat) * pow(sin(dLng / 2), 2.0)))
            }
        }
    }
}