package com.advisoryme.ibest.parents.ui.main

import android.Manifest
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import com.advisoryme.ibest.parents.data.models.Location
import com.advisoryme.ibest.parents.ui.base.BaseActivity
import com.advisoryme.ibest.parents.ui.notifications.NotificationsActivity
import com.advisoryme.ibest.parents.ui.shared.CircleTransformation
import com.advisoryme.ibest.parents.ui.splash.SplashActivity
import com.advisoryme.ibest.parents.utils.DialogUtils
import com.advisoryme.ibest.parents.utils.InjectionUtils
import com.advisoryme.ibest.parents.utils.MarkerUtils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlin.math.log


class MainActivity : BaseActivity<MainViewModel>() {
    private lateinit var mapFragment: SupportMapFragment
    private var markers: HashMap<Int, Marker> = HashMap()
    private var markersLocations = ArrayList<String>()
    private var zoomed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.advisoryme.ibest.parents.R.layout.activity_main)
        createOptionsMenu(com.advisoryme.ibest.parents.R.menu.menu_main)

        // customize map
        customizeMap()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        viewModel.updateFCMToken()
    }

    private fun customizeMap() {
        mapFragment = supportFragmentManager.findFragmentById(com.advisoryme.ibest.parents.R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync {
            if (isLocationPermissionGranted()) {
                it.isMyLocationEnabled = true
                it.setOnMyLocationChangeListener { location ->
                    val myCoordinate = LatLng(location.latitude, location.longitude)
                    val cameraUpdate = CameraUpdateFactory.newLatLngZoom(myCoordinate, MAP_ZOOM_LEVEL)
                    it.moveCamera(cameraUpdate)
                    it.setOnMyLocationChangeListener(null)
                }
            }

            // add marker click listener
            it.setOnMarkerClickListener { marker ->
                zoomToMarker(marker, 19f)
                return@setOnMarkerClickListener true
            }
        }
    }

    private fun isLocationPermissionGranted() = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED

    override fun onResume() {
        super.onResume()
        viewModel.startFetchingLocations()
    }

    override fun onPause() {
        super.onPause()
        viewModel.stopFetchingLocations()
    }

    override fun getTheViewModel() = InjectionUtils.provideMainViewModel(this)

    override fun observe() {
        super.observe()

        // locations event
        viewModel.locationsEvent.observe(this, Observer {
            if (it?.isNotEmpty() == true) {
                updateChildrenMarkers(it)
            }
        })

        // logout event
        viewModel.logoutEvent.observe(this, Observer {
            if (it == true) openSplashActivity()
        })
    }

    private fun updateChildrenMarkers(locations: List<Location>) {
        mapFragment.getMapAsync {
            // check locations size
            markersLocations.clear()
            if (locations.isEmpty()) {
                // remove all locations markers
                clearMarkers()
                return@getMapAsync
            }

            // create temp hash map to hold all markers
            val tempMarkers = HashMap<Int, Marker>()

            // loop all locations to add a new marker or just change its current marker
            for (location in locations) {
                val locationCoordinates = coordinateForMarker(location.lat, location.lng)

                // prepare location values
                val id = location.id
                val latLng = LatLng(locationCoordinates[0], locationCoordinates[1])

                // get location marker and check it
                var marker: Marker? = markers[id]
                if (marker != null) {
                    // marker found
                    // this location already has a marker on the map
                    // animate his marker
                    MarkerUtils.animateMarkerToGB(marker, latLng, MarkerUtils.LatLngInterpolator.LinearFixed())

                    // remove this marker from the drivers markers map
                    markers.remove(id)
                } else {
                    // marker not found
                    // this is a new location and doesn't has a marker on the map yet
                    // add new one for him
                    marker = it.addMarker(MarkerOptions()
                            .position(latLng)
                            .anchor(0.5f, 0.5f)
                            .flat(true))
                }

                if (marker != null) {
                    // load the marker image
                    loadMarkerIcon(marker, location)
                }

                // add the marker to the temp hash map
                tempMarkers[id] = marker as Marker
                markersLocations.add(locationCoordinates[0].toString() + "," + locationCoordinates[1].toString())
            }

            // now, remove all remaining markers
            clearMarkers()

            // assign temp marker map to current markers map because they are the current existing markers
            markers.clear()
            markers = tempMarkers

            // zoom to markers for the first time only
            zoomToMarkersForFirstTime()
        }
    }

    private fun zoomToMarkersForFirstTime() {
        if (zoomed) {
            return
        }

        mapFragment.getMapAsync {
            val builder = LatLngBounds.Builder()
            for (marker in markers.values) {
                builder.include(marker.position)
            }

            // check size
            if (markers.size > 1) {
                // zoom to all markers
                val bounds = builder.build()
                val padding = 0 // offset from edges of the map in pixels
                val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                it.animateCamera(cu)
            } else if (markers.size == 1) {
                // zoom to this marker
                for (marker in markers.values) {
                    zoomToMarker(marker, MAP_ZOOM_LEVEL)
                    break
                }
            }

            zoomed = true
        }
    }

    private fun zoomToMarker(marker: Marker, zoomLevel: Float) {
        mapFragment.getMapAsync {
            // zoom to the marker
            val position = marker.position

            val myCoordinate = LatLng(position.latitude, position.longitude)
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(myCoordinate, zoomLevel)
            it.animateCamera(cameraUpdate)
        }
    }

    private fun clearMarkers() {
        for (marker in markers.values) {
            marker.remove()
        }

        markers.clear()
    }

    private fun loadMarkerIcon(marker: Marker, location: Location) {
        if (location.icon.isNotEmpty()) {

            // load it
            Picasso.get().load(location.icon).transform(CircleTransformation()).resize(130, 130).into(object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                }

                override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
                }

                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if (bitmap != null) {
                        try {
                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap))

                        } catch (e: Exception) {
                            logE("Error: " + e.message)
                            e.printStackTrace()
                        }
                    }
                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            com.advisoryme.ibest.parents.R.id.action_notifications -> openNotificationsActivity()
            com.advisoryme.ibest.parents.R.id.action_change_password -> openChangePasswordDialog()
            com.advisoryme.ibest.parents.R.id.action_logout -> openLogoutConfirmDialog()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun openLogoutConfirmDialog() {
        DialogUtils.showConfirmDialog(this, com.advisoryme.ibest.parents.R.string.do_you_want_to_logout_q, DialogInterface.OnClickListener { _, _ -> viewModel.logout() })
    }

    private fun openNotificationsActivity() = startActivity(NotificationsActivity.newIntent(this))

    private fun openChangePasswordDialog() = ChangePasswordDialog(this).show()

    private fun openSplashActivity() {
        startActivity(SplashActivity.newIntent(this))
    }

    val COORDINATE_OFFSET = 0.00005f // You can change this value according to your need

    // Check if any marker is displayed on given coordinate. If yes then decide
    // another appropriate coordinate to display this marker. It returns an
    // array with latitude(at index 0) and longitude(at index 1).
    private fun coordinateForMarker(latitude: Double, longitude: Double): Array<Double> {

        val location = arrayOf(latitude, longitude)

        if (viewModel.locationsEvent.value == null) {
            return location
        }

        for (i in 0..viewModel.locationsEvent.value!!.size) {

            if (mapAlreadyHasMarkerForLocation((latitude + i * COORDINATE_OFFSET).toString()
                            + "," + (longitude + i * COORDINATE_OFFSET))) {

                // If i = 0 then below if condition is same as upper one. Hence, no need to execute below if condition.
                if (i == 0) {
                    continue
                }

                if (mapAlreadyHasMarkerForLocation((latitude - i * COORDINATE_OFFSET).toString()
                                + "," + (longitude - i * COORDINATE_OFFSET))) {
                    continue

                } else {
                    location[0] = latitude - i * COORDINATE_OFFSET
                    location[1] = longitude - i * COORDINATE_OFFSET
                    break
                }

            } else {
                location[0] = latitude + i * COORDINATE_OFFSET
                location[1] = longitude + i * COORDINATE_OFFSET
                break
            }
        }

        return location
    }

    // Return whether marker with same location is already on map
    private fun mapAlreadyHasMarkerForLocation(location: String): Boolean {
        return (markersLocations.contains(location))
    }

    companion object {
        private const val MAP_ZOOM_LEVEL = 16f

        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    or Intent.FLAG_ACTIVITY_NEW_TASK)

            return intent
        }
    }
}
