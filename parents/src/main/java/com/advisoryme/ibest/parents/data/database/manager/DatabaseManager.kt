package com.advisoryme.ibest.parents.data.database.manager

import android.arch.persistence.room.Room
import android.content.Context

object DatabaseManager {
    private var database: AppDatabase? = null

    fun getDatabase(context: Context): AppDatabase {
        if (database == null) {
            database = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java, "ibest-db"
            ).allowMainThreadQueries().build()
        }

        return database as AppDatabase
    }
}