package com.advisoryme.ibest.parents.ui.login

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.ui.base.BaseActivity
import com.advisoryme.ibest.parents.ui.main.MainActivity
import com.advisoryme.ibest.parents.utils.InjectionUtils
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity<LoginViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // add done listeners
        etNumber.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login()
            }
            true
        }
    }

    override fun getTheViewModel() = InjectionUtils.provideLoginViewModel(this)

    override fun observe() {
        super.observe()

        viewModel.loginEvent.observe(this, Observer {
            if (it == true) openMainActivity()
        })
    }

    fun onLogin(v: View) = login()

    fun login() = viewModel.login(etNumber.text.toString(), etPassword.text.toString())

    private fun openMainActivity() = startActivity(MainActivity.newIntent(this))

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }
}
