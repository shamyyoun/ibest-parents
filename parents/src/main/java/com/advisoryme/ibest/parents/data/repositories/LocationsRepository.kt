package com.advisoryme.ibest.parents.data.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.parents.data.apis.models.LocationsResponse
import com.advisoryme.ibest.parents.data.models.Location
import com.advisoryme.ibest.parents.utils.InjectionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationsRepository(token: String) : BaseRepository() {

    private val locationsApi = InjectionUtils.provideLocationsApi(token)

    fun locations(): LiveData<List<Location>> {
        val liveData = MutableLiveData<List<Location>>()
        val call = locationsApi.locations()

        call.enqueue(object : Callback<LocationsResponse> {
                override fun onFailure(call: Call<LocationsResponse>, t: Throwable) {
                liveData.value = null
            }

            override fun onResponse(call: Call<LocationsResponse>, response: Response<LocationsResponse>) {
                liveData.value = response.body()?.locations
            }
        })

        return liveData
    }
}
