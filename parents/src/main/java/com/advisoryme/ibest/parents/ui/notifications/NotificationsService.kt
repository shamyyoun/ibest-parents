package com.advisoryme.ibest.parents.ui.notifications

import com.advisoryme.ibest.parents.ui.splash.SplashActivity
import com.advisoryme.ibest.parents.utils.InjectionUtils
import com.advisoryme.ibest.parents.utils.Utils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class NotificationsService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // process the message
        try {
            val message = remoteMessage!!.data["message"] as String
            if (message.isNotEmpty()) {
                run {
                    // create the notification
                    val notification = InjectionUtils.proivdeNotification(message)

                    // check active user
                    val activeUserRepository = InjectionUtils.provideActiveUserRepository(this)
                    if (activeUserRepository.hasActiveUser()) {
                        // create notification repo and save the notification
                        val notificationsRepository = InjectionUtils.provideNotificationsRepository(this, activeUserRepository.getUser()!!.token)

                        notificationsRepository.saveNotification(notification)
                    }

                    // show notification
                    Utils.showNotification(this, message, intent = SplashActivity.newIntent(this))

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onNewToken(token: String?) {
        // log
        Utils.logE("New fcm token: $token")

        // check the token
        if (token != null) {
            // check active user
            val activeUserRepository = InjectionUtils.provideActiveUserRepository(this)
            if (activeUserRepository.hasActiveUser()) {
                // create notification repo and send the updated fcm token
                val notificationsRepository = InjectionUtils.provideNotificationsRepository(this, activeUserRepository.getUser()!!.token)

                run {
                    notificationsRepository.updateFCMToken(token).observeForever {
                        if (it?.result == true) {
                            notificationsRepository.setFCMTokenUpdated()
                        }
                    }
                }
            }
        }
    }
}