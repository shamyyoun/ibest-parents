package com.advisoryme.ibest.parents.ui.signup

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.data.models.User
import com.advisoryme.ibest.parents.ui.base.BaseViewModel
import com.advisoryme.ibest.parents.utils.InjectionUtils
import com.advisoryme.ibest.parents.utils.Utils

class SignupViewModel(app: Application) : BaseViewModel(app) {
    private val authenticationRepository = InjectionUtils.provideAuthenticationRepository()
    val signupEvent = MutableLiveData<Boolean>()
    val confirmCodeEvent = MutableLiveData<User>()

    fun signup(phoneNumber: String) {
        // validate
        if (validateInternetConnection() && validateSignupInputs(phoneNumber)) {
            triggerProgress()

            authenticationRepository.signup(phoneNumber).observeForever {
                triggerProgress()

                // process repo result then trigger the event
                handleRepositoryResult(it) {
                    signupEvent.value = it?.result
                }
            }
        }
    }

    fun confirmCode(phoneNumber: String, code: Int) {
        // validate
        if (validateInternetConnection() && validateConfirmCodeInputs(phoneNumber, code)) {
            triggerProgress()

            authenticationRepository.confirmCode(phoneNumber, code).observeForever {
                triggerProgress()

                // process repo result then trigger the event
                handleRepositoryResult(it) {
                    confirmCodeEvent.value = it?.result
                }
            }
        }
    }

    private fun validateSignupInputs(phoneNumber: String): Boolean {
        return when {
            phoneNumber.isEmpty() -> {
                triggerError(R.string.enter_phone_number)
                false
            }
            !Utils.isValidPhoneNumber(phoneNumber) -> {
                triggerError(R.string.invalid_phone_number)
                false
            }
            else -> {
                true
            }
        }
    }

    private fun validateConfirmCodeInputs(phoneNumber: String, code: Int): Boolean {
        return when {
            phoneNumber.isEmpty() -> {
                triggerError(R.string.enter_phone_number)
                false
            }
            !Utils.isValidPhoneNumber(phoneNumber) -> {
                triggerError(R.string.invalid_phone_number)
                false
            }
            code == 0 -> {
                triggerError(R.string.enter_verification_code)
                false
            }
            code < 999 -> {
                triggerError(R.string.invalid_verification_code)
                false
            }
            else -> true
        }
    }
}