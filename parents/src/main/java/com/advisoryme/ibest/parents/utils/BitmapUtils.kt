package com.advisoryme.ibest.parents.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.util.Base64
import com.advisoryme.ibest.parents.R
import java.io.*

/**
 * Created by Shamyyoun on 11/4/2015.
 * A utility class for resizing, downscaling, fixing orientation and saving bitmaps.
 */
object BitmapUtils {

    /**
     * creates a file with the specified name in the external storage directory.
     *
     * @param ctx      the context
     * @param fileName the file name to create
     * @return the file if created, or null otherwise
     */
    fun createImagePath(ctx: Context, fileName: String): File? {

        if (Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED) {
            Utils.showLongToast(ctx, R.string.unplug_mobile_cable_from_computer)
            return null
        }

        val directoryPath = File(Environment.getExternalStorageDirectory().path + Const.APP_FILES_DIR + "/images")

        if (!directoryPath.exists()) {
            if (!directoryPath.mkdirs()) {
                return null
            }
        }

        return File(directoryPath.toString() + String.format("/%s.jpg", fileName))
    }

    /**
     * Deletes all images in the images directory.
     */
    fun cleanImagesDirectory() {

        if (Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED) {
            return
        }

        val imagesDirectory = File(Environment.getExternalStorageDirectory().path + Const.APP_FILES_DIR + "/images")

        if (imagesDirectory.exists() && imagesDirectory.isDirectory) {
            for (file in imagesDirectory.listFiles()) {
                if (!file.isDirectory) file.delete()
            }
        }
    }

    /**
     * method, used to resize a bitmap keeping the same ratio and does'nt lose its ratios
     *
     * @param bitmapFilePath
     * @param maxDimen
     * @return
     */
    fun resizeBitmap(bitmapFilePath: String, maxDimen: Int): Bitmap? {
        val bitmap = BitmapFactory.decodeFile(bitmapFilePath)
        var width = bitmap.width
        var height = bitmap.height

        if (width > height) {
            if (width > maxDimen) {
                val ratio = maxDimen.toFloat() / width.toFloat()
                width = maxDimen
                height = (height.toFloat() * ratio).toInt()
            }
        } else {
            if (height > maxDimen) {
                val ratio = maxDimen.toFloat() / height.toFloat()
                height = maxDimen
                width = (width.toFloat() * ratio).toInt()
            }
        }

        return resizeBitmap(bitmapFilePath, width, height)
    }

    /**
     * Resize the bitmap which located in the specified path and respecting its aspect ratio, rotate it by Exif rotation and saveUser it in place.
     *
     * @param bitmapFilePath
     * @param width
     * @param height
     * @return the resized bitmap if successfully resized or null.
     */
    fun resizeBitmap(bitmapFilePath: String, width: Int, height: Int): Bitmap? {
        var bitmap = BitmapFactory.decodeFile(bitmapFilePath)

        val originalWidth = bitmap.width
        val originalHeight = bitmap.height

        var newWidth = -1
        var newHeight = -1
        var multFactor = -1.0f

        if (!(originalWidth < width && originalHeight < height)) { //don't scale up the bitmap
            if (originalHeight > originalWidth) {
                newHeight = height
                multFactor = originalWidth.toFloat() / originalHeight.toFloat()
                newWidth = (newHeight * multFactor).toInt()
            } else if (originalWidth > originalHeight) {
                newWidth = width
                multFactor = originalHeight.toFloat() / originalWidth.toFloat()
                newHeight = (newWidth * multFactor).toInt()
            } else if (originalHeight == originalWidth) {
                newHeight = height
                newWidth = width
            }
            val resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true)
            bitmap.recycle()
            bitmap = resizedBitmap
        }


        //resizing is before rotation for not consuming large space when rotating

        //handling exif rotation
        //http://stackoverflow.com/a/4105966
        //for more: https://gist.github.com/9re/1990019
        try {
            val exifData = ExifInterface(bitmapFilePath)
            val orientation = exifData.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            Utils.logE("Orientation: $orientation")

            val matrix = Matrix()
            var rotatedBitmap: Bitmap? = null
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> {
                    matrix.postRotate(90f)
                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                }
                ExifInterface.ORIENTATION_ROTATE_180 -> {
                    matrix.postRotate(180f)
                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                }
                ExifInterface.ORIENTATION_ROTATE_270 -> {
                    matrix.postRotate(270f)
                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                }
            }

            if (rotatedBitmap != null) { //bitmap has been rotated
                bitmap.recycle()
                bitmap = rotatedBitmap
            }

        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }

        saveBitmap(bitmap, bitmapFilePath, 80)

        return bitmap
    }


    /**
     * Resize the bitmap which located in the specified originalUri and respecting its aspect ratio, rotate it by Exif rotation and saveUser it in  a new path with the specified newFileName.
     *
     * @param originalUri
     * @param newFileName the name of the resized bitmap file
     * @param width
     * @param height
     * @return the resized bitmap File path or null.
     */
    fun resizeBitmap(ctx: Context, originalUri: Uri, newFileName: String, width: Int, height: Int): File? {

        val inputStream: InputStream?
        var bitmap: Bitmap
        try {
            inputStream = ctx.contentResolver.openInputStream(originalUri)
            bitmap = BitmapFactory.decodeStream(inputStream)
            inputStream?.close()
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

        val originalPath = FileUtils.getPath(ctx, originalUri) //the actual image path
        Utils.logE("path: ====== " + originalPath!!)
        if (originalPath == null)
            return null


        val originalWidth = bitmap.width
        val originalHeight = bitmap.height

        var newWidth = -1
        var newHeight = -1
        var multFactor = -1.0f

        if (!(originalWidth < width && originalHeight < height)) { //don't scale up the bitmap
            if (originalHeight > originalWidth) {
                newHeight = height
                multFactor = originalWidth.toFloat() / originalHeight.toFloat()
                newWidth = (newHeight * multFactor).toInt()
            } else if (originalWidth > originalHeight) {
                newWidth = width
                multFactor = originalHeight.toFloat() / originalWidth.toFloat()
                newHeight = (newWidth * multFactor).toInt()
            } else if (originalHeight == originalWidth) {
                newHeight = height
                newWidth = width
            }
            val resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true)
            bitmap.recycle()
            bitmap = resizedBitmap
        }


        //resizing is before rotation for not consuming large space when rotating

        //handling exif rotation
        //http://stackoverflow.com/a/4105966
        //for more: https://gist.github.com/9re/1990019
        try {
            val exifData = ExifInterface(originalPath)
            val orientation = exifData.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            Utils.logE("Orientation: $orientation")

            val matrix = Matrix()
            var rotatedBitmap: Bitmap? = null
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> {
                    matrix.postRotate(90f)
                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                }
                ExifInterface.ORIENTATION_ROTATE_180 -> {
                    matrix.postRotate(180f)
                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                }
                ExifInterface.ORIENTATION_ROTATE_270 -> {
                    matrix.postRotate(270f)
                    rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                }
            }

            if (rotatedBitmap != null) { //bitmap has been rotated
                bitmap.recycle()
                bitmap = rotatedBitmap
            }

        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }

        val newPath = createImagePath(ctx, newFileName)
        if (newPath != null)
            saveBitmap(bitmap, newPath.absolutePath, 80)

        return newPath
    }

    /**
     * Saves the bitmap to the specified path with the specified quality.
     *
     * @param bitmapFilePath
     * @param bitmap
     * @param quality
     * @return True if successfully saved.
     */
    fun saveBitmap(bitmap: Bitmap, bitmapFilePath: String, quality: Int): Boolean {
        val imageFile = File(bitmapFilePath)

        try {
            val out = FileOutputStream(imageFile)
            val compressed = bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)
            out.flush()
            out.close()
            return compressed
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }

    /**
     * method, used to encode image file to base64 string
     *
     * @param image
     * @return
     */
    fun encodeBase64(image: File): String {
        val bitmap = BitmapFactory.decodeFile(image.absolutePath)
        val os = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, os)
        val byteArr = os.toByteArray()

        return Base64.encodeToString(byteArr, Base64.DEFAULT)
    }

    /**
     * method, used to encode image file to base64 string with its type
     *
     * @param image
     * @return
     */
    fun encodeBase64WithType(image: File): String {
        var imageEncoded = BitmapUtils.encodeBase64(image)
        imageEncoded = String.format("data:image/%1\$s;base64,%2\$s", FileUtils.getExtension(image), imageEncoded)

        return imageEncoded
    }

    /**
     * method, used to decode base64 string to bitmap
     *
     * @param encodedImage
     * @return
     */
    fun decodeBase64(encodedImage: String): Bitmap {
        val decodedString = Base64.decode(encodedImage, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }

}
