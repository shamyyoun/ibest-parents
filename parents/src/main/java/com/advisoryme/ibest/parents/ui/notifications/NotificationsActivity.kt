package com.advisoryme.ibest.parents.ui.notifications

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.data.models.Notification
import com.advisoryme.ibest.parents.ui.base.BaseActivity
import com.advisoryme.ibest.parents.utils.InjectionUtils
import kotlinx.android.synthetic.main.activity_notifications.*

class NotificationsActivity : BaseActivity<NotificationsViewModel>() {
    private val adapter = NotificationsAdapter(listOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)

        // setup
        setupRecyclerView()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // fetch notifications
        viewModel.fetchNotifications()
    }

    private fun setupRecyclerView() {
        rvNotifications.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rvNotifications.adapter = adapter
    }

    override fun getTheViewModel() = InjectionUtils.provideNotificationsViewModel(this)

    override fun observe() {
        super.observe()

        // notifications event
        viewModel.notificationsEvent.observe(this, Observer {
            if (it != null) updateRVAdapter(it)
        })

        // empty event
        viewModel.emptyEvent.observe(this, Observer {
            if (it == true) {
                showShortToast(R.string.no_notifications_yet)
            }
        })
    }

    private fun updateRVAdapter(data: List<Notification>) {
        if (viewModel.notificationsEvent.value != null) {
            adapter.data = data
            adapter.notifyDataSetChanged()
        }
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, NotificationsActivity::class.java)
        }
    }
}
