package com.advisoryme.ibest.parents.data.models

import com.google.gson.annotations.SerializedName

data class Child(val name: String = "",
                 val photo: String = "",
                 val id: Int = 0,
                 @SerializedName("class")
                 val cls: String = "")