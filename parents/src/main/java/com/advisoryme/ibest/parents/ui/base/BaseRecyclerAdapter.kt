package com.advisoryme.ibest.parents.ui.base

import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.utils.DialogUtils
import com.advisoryme.ibest.parents.utils.Utils

/**
 * Created by Shamyyoun on 5/11/16.
 */
open abstract class BaseRecyclerAdapter<Item>(protected val context: Context, protected val data: MutableList<Item>)
    : RecyclerView.Adapter<BaseRecyclerViewHolder>() {
    private var itemClickListener: ((Int) -> Unit)? = null
    private var itemRemovedListener: ((Int) -> Unit)? = null
    private var progressDialog: AlertDialog? = null

    override fun getItemCount(): Int {
        return data.size
    }

    protected fun logE(msg: String) {
        Utils.logE(msg)
    }

    protected fun getString(resId: Int): String {
        return context.getString(resId)
    }

    fun showProgress() {
        when (progressDialog) {
            null -> progressDialog = DialogUtils.showProgressDialog(context, R.string.please_wait_dotted)
            else -> if (!(progressDialog?.isShowing!!)) {
                progressDialog?.show()
            }
        }
    }

    fun hideProgress() {
        progressDialog?.dismiss()
    }

    fun removeItem(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
        itemRemovedListener?.invoke(position)
    }

    fun hasInternetConnection() = Utils.hasConnection(context)

    protected fun showShortToast(msg: String) = Utils.showShortToast(context, msg)

    protected fun showShortToast(msgId: Int) = Utils.showShortToast(context, msgId)

    protected fun showLongToast(msg: String) = Utils.showLongToast(context, msg)

    protected fun showLongToast(msgId: Int) = Utils.showLongToast(context, msgId)
}
