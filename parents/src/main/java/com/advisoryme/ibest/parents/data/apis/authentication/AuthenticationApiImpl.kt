package com.advisoryme.ibest.parents.data.apis.authentication

import com.advisoryme.ibest.parents.data.apis.utils.ApiInjectionUtils
import com.advisoryme.ibest.parents.data.models.User
import com.advisoryme.ibest.parents.utils.InjectionUtils
import retrofit2.Call

class AuthenticationApiImpl {
    private val authenticationApi: AuthenticationApi

    init {
        val apiManagerImpl = InjectionUtils.provideApiManager()
        authenticationApi = apiManagerImpl.createApi(AuthenticationApi::class.java)
    }

    fun signup(phoneNumber: String): Call<Void> {
        val body = ApiInjectionUtils.provideSignupBody(phoneNumber)
        return authenticationApi.signup(body)
    }

    fun confirmCode(phoneNumber: String, code: Int): Call<User> {
        val body = ApiInjectionUtils.provideConfirmCodeBody(phoneNumber, code)
        return authenticationApi.confirmCode(body)
    }

    fun login(phoneNumber: String, password: String): Call<User> {
        val body = ApiInjectionUtils.provideLoginBody(phoneNumber, password)
        return authenticationApi.login(body)

    }
}