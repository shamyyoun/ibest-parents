package com.advisoryme.ibest.parents.data.repositories

import android.arch.lifecycle.LiveData
import com.advisoryme.ibest.parents.data.models.RepositoryResult
import com.advisoryme.ibest.parents.utils.InjectionUtils

class UpdatePasswordRepository(token: String) : BaseRepository() {
    private val updatePasswordApi = InjectionUtils.provideUpdatePasswordApi(token)

    fun changePassword(newPassword: String, oldPassword: String = ""): LiveData<RepositoryResult<Boolean>> {
        val call = updatePasswordApi.changePassword(newPassword, oldPassword)
        return handleApiResponse(call)
    }
}