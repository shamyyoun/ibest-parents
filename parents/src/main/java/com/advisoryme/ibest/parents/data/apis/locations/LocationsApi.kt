package com.advisoryme.ibest.parents.data.apis.locations

import com.advisoryme.ibest.parents.data.apis.models.LocationsResponse
import retrofit2.Call
import retrofit2.http.GET

interface LocationsApi {
    @GET("locations")
    fun locations(): Call<LocationsResponse>
}