package com.advisoryme.ibest.parents.ui.base

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.utils.DialogUtils
import com.advisoryme.ibest.parents.utils.Utils
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {
    private lateinit var rootView: FrameLayout
    private var progressDialog: AlertDialog? = null
    private var toolbar: Toolbar? = null
    private var tvToolbarTitle: TextView? = null
    private var menuId: Int = 0
    private var enableBack: Boolean = false
    private var iconResId: Int = 0
    private var toolbarTitle: String? = title?.toString()
    protected lateinit var viewModel: VM

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        rootView = findViewById(android.R.id.content)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // init the toolbar
        val viewToolbar = findViewById<ViewGroup>(R.id.viewToolbar)
        if (viewToolbar != null) {
            toolbar = viewToolbar.findViewById(R.id.toolbar)
        }

        // check the toolbar
        if (toolbar != null) {
            // toolbar is available >> handle it
            setSupportActionBar(toolbar)
            toolbar?.title = ""
            tvToolbarTitle = toolbar?.findViewById(R.id.tvToolbarTitle)
            tvToolbarTitle?.text = title

            // check if enable back
            if (enableBack) {
                // set the suitable back icon
                toolbar?.setNavigationIcon(if (iconResId != 0) iconResId else R.drawable.white_back_icon)
            } else if (iconResId != 0) {
                // set this icon
                toolbar?.setNavigationIcon(iconResId)
            }
        }

        // get viewModel & observe
        viewModel = getTheViewModel()
        observe()
    }

    protected abstract fun getTheViewModel(): VM

    protected open fun observe() {
        // progress event
        viewModel.progressEvent.observe(this, Observer {
            when (it) {
                true -> showProgress()
                else -> hideProgress()
            }
        })

        // error event
        viewModel.errorEvent.observe(this, Observer {
            if (it != null) showShortToast(it)
        })

        // hide keyboard event
        viewModel.hideKeyboardEvent.observe(this, Observer {
            if (it != null && it) {
                hideKeyboard()
            }
        })
    }

    protected fun logE(msg: String) = Utils.logE(msg)

    protected open fun showProgress() {
        when (progressDialog) {
            null -> progressDialog = DialogUtils.showProgressDialog(this, R.string.please_wait_dotted)
            else -> if (progressDialog?.isShowing == false) {
                progressDialog?.show()
            }
        }
    }

    protected open fun hideProgress() = progressDialog?.dismiss()

    protected fun getResColor(id: Int) = resources.getColor(id)

    internal fun loadFragment(fragment: Fragment, container: Int = R.id.container, enterAnim: Int = 0, exitAnim: Int = 0) {
        val ft = supportFragmentManager.beginTransaction()
        if (enterAnim != 0 && exitAnim != 0) {
            ft.setCustomAnimations(enterAnim, exitAnim)
        }
        ft.replace(container, fragment)
        ft.commitAllowingStateLoss()
    }

    protected fun hideKeyboard() = Utils.hideKeyboard(rootView)

    override fun setTitle(title: CharSequence) {
        toolbarTitle = title.toString()
        when (tvToolbarTitle) {
            null -> super.setTitle(title)
            else -> tvToolbarTitle?.text = title
        }
    }

    override fun setTitle(titleId: Int) {
        toolbarTitle = getString(titleId)
        when (tvToolbarTitle) {
            null -> super.setTitle(titleId)
            else -> tvToolbarTitle?.setText(titleId)
        }
    }

    protected fun hideToolbarTitle() {
        tvToolbarTitle?.visibility = View.GONE
    }

    protected fun showToolbarTitle() {
        tvToolbarTitle?.visibility = View.VISIBLE
    }

    internal fun createOptionsMenu(menuId: Int) {
        this.menuId = menuId
        invalidateOptionsMenu()
    }

    internal fun removeOptionsMenu() {
        menuId = 0
        invalidateOptionsMenu()
    }

    protected fun enableBackButton() {
        enableBack = true
    }

    protected fun setToolbarIcon(iconResId: Int) {
        this.iconResId = iconResId
    }

    override fun onCreateOptionsMenu(menu: Menu) =
            when (menuId) {
                0 -> super.onCreateOptionsMenu(menu)
                else -> {
                    menuInflater.inflate(menuId, menu)
                    true
                }
            }

    override fun onOptionsItemSelected(item: MenuItem) =
            when {
                item.itemId == android.R.id.home && enableBack -> {
                    onBackPressed()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    internal fun hasToolbar() = toolbar != null

    protected fun hasInternetConnection() = Utils.hasConnection(this)

    protected fun showShortToast(msg: String) = Utils.showShortToast(this, msg)

    protected fun showShortToast(msgId: Int) = Utils.showShortToast(this, msgId)

    protected fun showLongToast(msg: String) = Utils.showLongToast(this, msg)

    protected fun showLongToast(msgId: Int) = Utils.showLongToast(this, msgId)
}