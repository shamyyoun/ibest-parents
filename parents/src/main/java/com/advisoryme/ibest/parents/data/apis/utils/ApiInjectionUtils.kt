package com.advisoryme.ibest.parents.data.apis.utils

import com.advisoryme.ibest.parents.data.apis.bodies.*

object ApiInjectionUtils {

    fun provideSignupBody(phoneNumber: String) = SignupBody(phoneNumber)

    fun provideConfirmCodeBody(phoneNumber: String, code: Int) = ConfirmCodeBody(phoneNumber, code)

    fun provideChangePasswordBody(newPassword: String, oldPassword: String) = ChangePasswordBody(newPassword, oldPassword)

    fun provideLoginBody(phoneNumber: String, password: String) = LoginBody(phoneNumber, password)

    fun provideUpdateFCMTokenBody(fcmToken: String) = UpdateFCMTokenBody(fcmToken)
}