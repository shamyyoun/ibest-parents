package com.advisoryme.ibest.parents.data.prefs

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.advisoryme.ibest.parents.utils.Const
import com.google.gson.Gson

/**
 * A utility class that saves serialized objects (using Gson) in the SharedPreferences.
 *
 * @param <T> the type of the object to saveUser.
</T> */
class PrefsManagerImpl(context: Context) : PrefsManager {
    private val prefs: SharedPreferences = context.getSharedPreferences(Const.PREFS_NAME, Context.MODE_PRIVATE)

    override fun <T> save(obj: T, key: String) {
        // prepare data
        val data = when (obj) {
            is String -> obj
            else -> Gson().toJson(obj)
        }

        // saveUser data in sp
        prefs.edit()
                .putString(key, data)
                .apply()
        Log.e(TAG, "Data saved with key: $key\nData: $data")
    }

    override fun <T> load(key: String, cls: Class<*>): T? {
        val data = prefs.getString(key, "")
        Log.e(TAG, "Data loaded with key: $key\nData: $data")

        // return the value according to generics
        return when {
            cls == String::class.java -> data as T
            data.isEmpty() -> null
            else -> Gson().fromJson<T>(data, cls)
        }
    }

    // removes a key from the preferences
    override fun remove(key: String) {
        prefs.edit().remove(key).apply()
        Log.e(TAG, "Data removed with key: $key")
    }

    companion object {
        private const val TAG = "PrefsManager"
    }
}
