package com.advisoryme.ibest.parents.ui.login

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.parents.R
import com.advisoryme.ibest.parents.ui.base.BaseViewModel
import com.advisoryme.ibest.parents.utils.InjectionUtils

class LoginViewModel(app: Application) : BaseViewModel(app) {
    private val authenticationRepository = InjectionUtils.provideAuthenticationRepository()
    private val activeUserRepository = InjectionUtils.provideActiveUserRepository(app)
    val loginEvent = MutableLiveData<Boolean>()

    fun login(phoneNumber: String, password: String) {
        if (validateInternetConnection() && validateLoginInputs(phoneNumber, password)) {
            triggerProgress()

            authenticationRepository.login(phoneNumber, password).observeForever {
                triggerProgress()

                handleRepositoryResult(it) {
                    activeUserRepository.updateUser(it?.result!!)
                    loginEvent.value = true
                }
            }
        }
    }

    private fun validateLoginInputs(phoneNumber: String, password: String): Boolean {
        return when {
            phoneNumber.isEmpty() -> {
                triggerError(R.string.enter_phone_number)
                false
            }
            password.isEmpty() -> {
                triggerError(R.string.enter_password)
                false
            }
            else -> true
        }
    }
}