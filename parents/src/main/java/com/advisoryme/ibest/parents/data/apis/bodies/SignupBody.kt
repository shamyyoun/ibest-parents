package com.advisoryme.ibest.parents.data.apis.bodies

import com.google.gson.annotations.SerializedName

data class SignupBody(
        @SerializedName("phone_number")
        val phoneNumber: String = ""
)