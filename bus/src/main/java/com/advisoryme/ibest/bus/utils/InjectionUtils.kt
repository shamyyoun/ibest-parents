package com.advisoryme.ibest.bus.utils

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.advisoryme.ibest.bus.data.models.RepositoryResult
import com.advisoryme.ibest.bus.data.network.ApiManagerImpl
import com.advisoryme.ibest.bus.data.network.location.LocationApiImpl
import com.advisoryme.ibest.bus.data.prefs.PrefsManagerImpl
import com.advisoryme.ibest.bus.data.repositories.LocationRepository
import com.advisoryme.ibest.bus.ui.main.MainViewModel
import com.advisoryme.ibest.bus.ui.shared.ViewModelCustomProvider
import okhttp3.Interceptor

object InjectionUtils {
    fun providePrefsManager(context: Context) = PrefsManagerImpl(context)

    fun provideApiManager(vararg interceptors: Interceptor) = ApiManagerImpl.instance(*interceptors)

    fun provideLocationRepository(context: Context) = LocationRepository(context)

    fun provideLocationApi() = LocationApiImpl()

    fun <T> provideRepositoryResult(result: T? = null, message: String? = null) = RepositoryResult(result, message)

    fun provideMainViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(
                    activity,
                    ViewModelCustomProvider(MainViewModel(activity.application)))
                    .get(MainViewModel::class.java)
}