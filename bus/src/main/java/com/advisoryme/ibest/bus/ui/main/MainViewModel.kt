package com.advisoryme.ibest.bus.ui.main

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.bus.data.models.events.LocationServiceStatusChangedEvent
import com.advisoryme.ibest.bus.ui.base.BaseViewModel
import com.advisoryme.ibest.bus.utils.InjectionUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class MainViewModel(app: Application) : BaseViewModel(app) {
    private val locationRepository = InjectionUtils.provideLocationRepository(app)
    val locationEvent = MutableLiveData<Boolean>()

    init {
        locationEvent.value = locationRepository.getLocationServiceStatus()
        EventBus.getDefault().register(this)
    }

    fun startLocationUpdates() = locationRepository.startLocationUpdates()

    fun stopLocationUpdates() = locationRepository.stopLocationUpdates()

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLocationServiceStatusChangedEvent(event: LocationServiceStatusChangedEvent) {
        locationEvent.value = event.status
    }

    override fun onCleared() {
        super.onCleared()

        EventBus.getDefault().unregister(this)
    }
}