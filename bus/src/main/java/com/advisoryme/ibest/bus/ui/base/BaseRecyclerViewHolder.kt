package com.advisoryme.ibest.bus.ui.base

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Shamyyoun on 5/11/16.
 */
open class BaseRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private var clickableRootView: View? = null // this is used to change the default onItemClickListener

    internal fun setOnItemClickListener(itemClickListener: (Int) -> Unit) {
        if (clickableRootView != null) {
            clickableRootView?.setOnClickListener { itemClickListener(adapterPosition) }
        } else {
            itemView.setOnClickListener { itemClickListener(adapterPosition) }
        }
    }

    protected fun findViewById(viewId: Int): View? {
        return itemView.findViewById(viewId)
    }
}
