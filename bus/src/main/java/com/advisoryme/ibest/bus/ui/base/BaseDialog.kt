package com.advisoryme.ibest.bus.ui.base

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import com.advisoryme.ibest.bus.R
import com.advisoryme.ibest.bus.utils.Utils

/**
 * Created by Shamyyoun on 2/17/2016.
 */
abstract class BaseDialog<VM : BaseViewModel>(protected val activity: AppCompatActivity) : Dialog(activity) {
    private var rootView: FrameLayout? = null
    private var tvDialogTitle: TextView? = null
    private var ibClose: ImageButton? = null
    private var progressView: View? = null
    protected lateinit var viewModel: VM

    init {
        // set no title and transparent bg
        requestWindowFeature(Window.FEATURE_NO_TITLE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // get viewModel & observe
        viewModel = getTheViewModel()
        observe()
    }

    override fun setContentView(layoutResId: Int) {
        super.setContentView(layoutResId)

        // init and customize the dialog toolbar
        rootView = findViewById(android.R.id.content)
        val toolbar = findViewById<View>(R.id.toolbar)
        if (toolbar != null) {
            tvDialogTitle = findViewById(R.id.tvDialogTitle)
            ibClose = findViewById(R.id.ibClose)
            ibClose?.setOnClickListener { dismiss() }
        }
    }

    protected abstract fun getTheViewModel(): VM

    protected open fun observe() {
        // progress event
        viewModel.progressEvent.observe(activity, Observer {
            when (it) {
                true -> showProgress()
                else -> hideProgress()
            }
        })

        // error event
        viewModel.errorEvent.observe(activity, Observer {
            if (it != null) showShortToast(it)
        })

        // hide keyboard event
        viewModel.hideKeyboardEvent.observe(activity, Observer {
            if (it != null && it) {
                hideKeyboard()
            }
        })
    }

    override fun setTitle(title: CharSequence?) {
        tvDialogTitle?.text = title
    }

    override fun setTitle(titleId: Int) = setTitle(getString(titleId))

    override fun setCancelable(flag: Boolean) {
        super.setCancelable(flag)
        ibClose?.visibility = if (flag) View.VISIBLE else View.GONE
    }

    protected fun logE(msg: String) = Utils.logE(msg)

    protected fun getString(strId: Int) = context.getString(strId)

    protected fun showProgress() {
        hideProgress()

        if (rootView != null && progressView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            progressView = inflater.inflate(R.layout.view_dialog_progress, null)
            rootView?.addView(progressView)
        }
        progressView?.visibility = View.VISIBLE
        super.setCancelable(false)
    }

    protected fun hideProgress() {
        progressView?.visibility = View.GONE
        super.setCancelable(true)
    }

    override fun dismiss() {
        // hide keyboard
        hideKeyboard()

        super.dismiss()
    }

    protected fun hideKeyboard() {
        if (rootView != null) {
            Utils.hideKeyboard(rootView!!)
        }
    }

    override fun show() {
        super.show()

        // customize the dialog width
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(window?.attributes)
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        window?.attributes = layoutParams
    }

    protected fun hasInternetConnection() = Utils.hasConnection(context)

    protected fun showShortToast(msg: String) = Utils.showShortToast(context, msg)

    protected fun showShortToast(msgId: Int) = Utils.showShortToast(context, msgId)

    protected fun showLongToast(msg: String) = Utils.showLongToast(context, msg)

    protected fun showLongToast(msgId: Int) = Utils.showLongToast(context, msgId)
}
