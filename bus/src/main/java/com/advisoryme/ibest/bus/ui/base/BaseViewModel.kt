package com.advisoryme.ibest.bus.ui.base

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.advisoryme.ibest.bus.R
import com.advisoryme.ibest.bus.data.models.RepositoryResult
import com.advisoryme.ibest.bus.utils.Utils

open class BaseViewModel(protected val app: Application) : AndroidViewModel(app) {
    val progressEvent = MutableLiveData<Boolean>()
    val errorEvent = MutableLiveData<String>()
    val hideKeyboardEvent = MutableLiveData<Boolean>()

    protected fun logE(msg: String) = Utils.logE(msg)

    protected fun hasInternetConnection() = Utils.hasConnection(app)

    protected fun validateInternetConnection(): Boolean {
        return if (hasInternetConnection()) {
            true
        } else {
            triggerHideKeyboard()
            triggerError(R.string.no_internet_connection)
            false
        }
    }

    protected fun getString(strId: Int): String = app.getString(strId)

    protected fun triggerProgress(show: Boolean = if (progressEvent.value != null) !progressEvent.value!! else true) {
        // check to trigger hide keyboard
        if (show) triggerHideKeyboard()

        // progress event
        progressEvent.value = show
    }

    protected fun triggerError(msgResId: Int = R.string.something_went_wrong) {
        triggerError(getString(msgResId))
    }

    protected fun triggerError(msg: String?) {
        errorEvent.value = msg ?: getString(R.string.something_went_wrong)
    }

    protected fun triggerHideKeyboard() {
        hideKeyboardEvent.value = true
    }

    protected fun <T> handleRepositoryResult(repositoryResult: RepositoryResult<T>?,
                                             successCallback: (() -> Unit)? = null): Boolean {

        return if (repositoryResult != null) {
            if (repositoryResult.
                            result == null) {
                triggerError(repositoryResult.message)
                false
            } else {
                successCallback?.invoke()
                true
            }
        } else {
            triggerError()
            false
        }
    }
}