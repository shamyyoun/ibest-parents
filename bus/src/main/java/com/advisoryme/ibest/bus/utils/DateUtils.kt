package com.advisoryme.ibest.bus.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Shamyyoun on 2/22/2015.
 */
object DateUtils {
    public const val SERVER_DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss"
    public const val SERVER_DATE_FORMAT = "yyyy-MM-dd"
    public const val SERVER_TIME_FORMAT = "HH:mm:ss"

    val currentTime: String?
        @Deprecated("")
        get() {
            val calendar = Calendar.getInstance(Locale.getDefault())
            return convertToString(calendar, "hh:mm:ss")
        }

    /**
     * returns current time in "HH:mm:ss" format
     */
    val currentTime24HrFormat: String?
        get() {
            val calendar = Calendar.getInstance(Locale.getDefault())
            return convertToString(calendar, "HH:mm:ss")
        }

    fun convertToCalendar(strDate: String?, strFormat: String): Calendar? {
        try {
            val calendar = Calendar.getInstance(Locale.getDefault())
            val df = SimpleDateFormat(strFormat, Locale.ENGLISH)
            calendar.time = df.parse(strDate)

            return calendar
        } catch (e: Exception) {
            return null
        }

    }

    fun convertToCalendar(millis: Long): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return calendar
    }

    @JvmOverloads
    fun convertToString(calendar: Calendar?, strFormat: String, locale: Locale = Locale.ENGLISH): String? {
        try {
            val format = SimpleDateFormat(strFormat, locale)
            return format.format(calendar!!.time)
        } catch (e: Exception) {
            return null
        }

    }

    fun convertToMillies(strDate: String, strFormat: String): Long {
        val calendar = convertToCalendar(strDate, strFormat)
        return calendar?.time?.time ?: 0
    }

    @JvmOverloads
    fun formatDate(strDate: String, originalFormat: String, desiredFormat: String, locale: Locale = Locale.ENGLISH): String? {
        return convertToString(convertToCalendar(strDate, originalFormat), desiredFormat, locale)
    }

    fun formatDate(date: Long, desiredFormat: String, locale: Locale = Locale.ENGLISH): String? {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date
        return convertToString(calendar, desiredFormat, locale)
    }

    fun getDayName(date: String, dateFormat: String): String {
        val calendar = convertToCalendar(date, dateFormat)
        return getDayName(calendar!!)
    }

    fun getDayName(date: String, dateFormat: String, locale: Locale): String {
        val calendar = convertToCalendar(date, dateFormat)
        return getDayName(calendar!!, locale)
    }

    fun getShortDayName(date: String, dateFormat: String): String {
        val calendar = convertToCalendar(date, dateFormat)
        return getShortDayName(calendar)
    }

    fun getShortDayName(date: String, dateFormat: String, locale: Locale): String {
        val calendar = convertToCalendar(date, dateFormat)
        return getShortDayName(calendar, locale)
    }

    @JvmOverloads
    fun getShortDayName(date: Calendar?, locale: Locale = Locale.ENGLISH): String {
        return getDayName(date!!, locale, true)
    }

    @JvmOverloads
    fun getDayName(date: Calendar, locale: Locale = Locale.ENGLISH, shortName: Boolean = false): String {
        val sdf = SimpleDateFormat(if (shortName) "E" else "EEEE", locale)

        return sdf.format(date.time)
    }

    fun isCurrentDate(calendar: Calendar): Boolean {
        val currentCalendar = Calendar.getInstance(Locale.getDefault())
        return calendar.get(Calendar.DAY_OF_MONTH) == currentCalendar.get(Calendar.DAY_OF_MONTH) && calendar.get(Calendar.MONTH) == currentCalendar.get(Calendar.MONTH) && calendar.get(Calendar.YEAR) == currentCalendar.get(Calendar.YEAR)
    }

    fun isCurrentDate(date: String, dateFormat: String): Boolean {
        val calendar = convertToCalendar(date, dateFormat)
        return isCurrentDate(calendar!!)
    }

    fun isPastDate(calendar: Calendar): Boolean {
        val currentCalendar = Calendar.getInstance(Locale.getDefault())
        return calendar.timeInMillis < currentCalendar.timeInMillis
    }

    fun isPastDate(strDate: String, strFormat: String): Boolean {
        val calendar = convertToCalendar(strDate, strFormat)
        return isPastDate(calendar!!)
    }

    fun isPastTime(strTime: String, strFormat: String): Boolean {
        val currentTime = getCurrentDate(strFormat)
        return compare(strTime, currentTime, strFormat) < 1
    }

    fun isToday(calendar: Calendar): Boolean {
        val currentCalendar = Calendar.getInstance(Locale.getDefault())
        return (currentCalendar.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)
                && currentCalendar.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                && currentCalendar.get(Calendar.YEAR) == calendar.get(Calendar.YEAR))
    }

    fun isToday(strDate: String, strFormat: String): Boolean {
        val calendar = convertToCalendar(strDate, strFormat)
        return isToday(calendar!!)
    }

    fun getNewStringDate(strDate: String, strFormat: String, daysToAdd: Int): String? {
        val calendar = addDays(strDate, strFormat, daysToAdd)
        return convertToString(calendar, strFormat)
    }

    fun addDays(strDate: String, strFormat: String, daysToAdd: Int): Calendar? {
        val calendar = convertToCalendar(strDate, strFormat)
        if (calendar != null) {
            calendar.add(Calendar.DATE, daysToAdd)
            return calendar
        } else {
            return null
        }
    }

    fun getCurrentDatePlusDays(strFormat: String, daysToAdd: Int): String? {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, daysToAdd)

        return convertToString(calendar, strFormat)
    }

    fun addDays(daysToAdd: Int): Calendar {
        val calendar = Calendar.getInstance(Locale.getDefault())
        calendar.add(Calendar.DATE, daysToAdd)
        return calendar
    }

    fun addHours(strDate: String, strFormat: String, hoursToAdd: Int): Calendar {
        val calendar = convertToCalendar(strDate, strFormat)
        calendar!!.add(Calendar.HOUR, hoursToAdd)
        return calendar
    }

    /**
     * method, used to compare two dates
     *
     * @param strDate1
     * @param strDate2
     * @param dateFormat
     * @return 1 if date1 is after date2, -1 if date1 is before date2 and 0 if the the dates are the same
     */
    fun compare(strDate1: String, strDate2: String?, dateFormat: String): Int {
        val calendar1 = convertToCalendar(strDate1, dateFormat)
        val calendar2 = convertToCalendar(strDate2, dateFormat)

        return calendar1!!.compareTo(calendar2)
    }

    /**
     * Requires: strDate2 to be later than strDate1
     */
    fun difference(strDate1: String, strDate2: String, dateFormat: String): Long {
        val calMillis1 = convertToCalendar(strDate1, dateFormat)!!.timeInMillis
        val calMillis2 = convertToCalendar(strDate2, dateFormat)!!.timeInMillis

        return calMillis2 - calMillis1
    }

    fun getLocalizedDate(date: String): String {
        var date = date
        try {
            // check language
            val lang = Utils.appLanguage
            if ("ar" == lang) {
                // arabic
                // format date to be arabic
                date = date.toLowerCase().replace("am", "ص").replace("pm", "م")
            } else {
                // not arabic,
                // mostly it should be english
                // format ∂ate to be english
                date = date.replace("ص", "AM").replace("م", "PM")
            }

        } catch (e: Exception) {
        }

        return date
    }

    fun getCurrentDate(strFormat: String): String? {
        val calendar = Calendar.getInstance(Locale.getDefault())
        return convertToString(calendar, strFormat)
    }

    fun getCurrentDateInMillis(): Long {
        val calendar = Calendar.getInstance(Locale.getDefault())
        return calendar.timeInMillis
    }
}
