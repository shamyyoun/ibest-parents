package com.advisoryme.ibest.bus.data.network.bodies

import com.google.gson.annotations.SerializedName

data class UpdateLocationBody(
        val lat: String = "",
        @SerializedName("long")
        val lng: String = "",
        val speed: String = "",
        val distance: String = "",
        val timestamp: String = "",
        @SerializedName("device_id")
        private val deviceId: String = "a1"
)