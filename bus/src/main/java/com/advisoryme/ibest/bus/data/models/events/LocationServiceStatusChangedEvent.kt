package com.advisoryme.ibest.bus.data.models.events

data class LocationServiceStatusChangedEvent(val status: Boolean)