package com.advisoryme.ibest.bus.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

/**
 * Created by Shamyyoun on 11/12/16.
 */

object LocationUtils {
    const val REQ_LOCATION_PERMISSIONS = 1000

    /**
     * method, used to check if gps  is enabled on the device or not
     *
     * @param context
     * @return boolean variable
     */
    fun checkGps(context: Context): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    /**
     * method, used to open location settings activity
     *
     * @param context
     */
    fun openLocationSettingsActivity(context: Context) {
        val intent = Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        context.startActivity(intent)
    }

    /**
     * method, used to get location from GPS
     *
     * @param context
     *
     * @return
     */
    fun getLastKnownLocation(context: Context): Location? {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
    }

    /**
     * method, used to check if location permissions are granted or not
     */
    fun checkLocationPermissions(context: Context) =
            ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED

    fun requuireLocationPermissions(activity: Activity) {
        ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION),
                REQ_LOCATION_PERMISSIONS)
    }
}
