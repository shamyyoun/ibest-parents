package com.advisoryme.ibest.bus.data.network.utils

import com.advisoryme.ibest.bus.data.network.bodies.UpdateLocationBody

object ApiInjectionUtils {

    fun provideUpdateLocationBody(lat: Double, lng: Double, speed: Int, distance: Float, timestamp: Long) =
            UpdateLocationBody(lat.toString(), lng.toString(), speed.toString(), distance.toString(), timestamp.toString())
}