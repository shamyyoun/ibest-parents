package com.advisoryme.ibest.bus.data.network

import com.advisoryme.ibest.bus.utils.Const
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManagerImpl : ApiManager {
    private lateinit var retrofit: Retrofit
    private val retrofitBuilder = Retrofit.Builder()

    constructor(vararg interceptors: Interceptor) {
        retrofitBuilder.baseUrl(Const.END_POINT)
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create())

        // build the httpOk client
        buildClient(*interceptors)
    }

    override fun <T> createApi(apiClass: Class<T>): T {
        retrofit = retrofitBuilder.build()
        return retrofit.create(apiClass)
    }

    override fun buildClient(vararg interceptors: Interceptor) {
        val httpOk = OkHttpClient.Builder()

        // add basic interceptors
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        httpOk.addInterceptor(interceptor)

        // add user defined interceptors
        for (interceptor in interceptors) {
            httpOk.addInterceptor(interceptor)
        }

        // set client
        retrofitBuilder.client(httpOk.build())
    }

    companion object {
        private var apiManagerImpl: ApiManagerImpl? = null

        fun instance(vararg interceptors: Interceptor): ApiManagerImpl {
            if (apiManagerImpl == null) {
                apiManagerImpl = ApiManagerImpl(*interceptors)
            } else if (interceptors.isNotEmpty()) {
                apiManagerImpl?.buildClient(*interceptors)
            }

            return apiManagerImpl!!
        }
    }
}