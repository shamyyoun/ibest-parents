package com.advisoryme.ibest.bus.utils

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.advisoryme.ibest.bus.R
import dmax.dialog.SpotsDialog

/**
 * Created by Shamyyoun on 2/24/2016.
 */
object DialogUtils {
    /**
     * method, used to show alert dialog with passed message res id
     *
     * @param context
     * @param messageResId
     * @param onClickListener
     * @return
     */
    fun showAlertDialog(context: Context, messageResId: Int, onClickListener: DialogInterface.OnClickListener): AlertDialog {
        return showAlertDialog(context, context.getString(messageResId), onClickListener)
    }

    /**
     * method, used to alert dialog with passed message string
     *
     * @param context
     * @param message
     * @param onClickListener
     */
    fun showAlertDialog(context: Context, message: String, onClickListener: DialogInterface.OnClickListener?): AlertDialog {
        // create the dialog builder & set message
        val dialogBuilder = AlertDialog.Builder(context, R.style.MyDialog)
        dialogBuilder.setMessage(message)

        // check the click listener
        if (onClickListener != null) {
            // not null
            // add positive click listener
            dialogBuilder.setPositiveButton(context.getString(R.string.ok), onClickListener)
        } else {
            // null
            // add new click listener to dismiss the dialog
            dialogBuilder.setPositiveButton(context.getString(R.string.ok)) { dialog, which -> dialog.dismiss() }
        }

        // create and show the dialog
        val dialog = dialogBuilder.create()
        dialog.show()
        customizeDialogMsgTextView(dialog)

        return dialog
    }

    /**
     * method, used to show confirm dialog with passed message res id
     *
     * @param context
     * @param messageResId
     * @param positiveClickListener
     */
    fun showConfirmDialog(context: Context, messageResId: Int, positiveClickListener: DialogInterface.OnClickListener,
                          negativeClickListener: DialogInterface.OnClickListener? = null): AlertDialog {

        return showConfirmDialog(context, context.getString(messageResId), positiveClickListener, negativeClickListener)
    }

    /**
     * method, used to show confirm dialog with passed message and buttons text res ids
     *
     * @param context
     * @param messageResId
     * @param positiveClickListener
     */
    fun showConfirmDialog(context: Context, messageResId: Int,
                          positiveClickListener: DialogInterface.OnClickListener, positiveButtonTextResId: Int,
                          negativeClickListener: DialogInterface.OnClickListener, negativeButtonTextResId: Int): AlertDialog {

        return showConfirmDialog(context, context.getString(messageResId), positiveClickListener,
                context.getString(positiveButtonTextResId), negativeClickListener, context.getString(negativeButtonTextResId))
    }

    /**
     * method, used to show confirm dialog with passed message string
     *
     * @param context
     * @param message
     * @param positiveClickListener
     */
    fun showConfirmDialog(context: Context, message: String,
                          positiveClickListener: DialogInterface.OnClickListener,
                          negativeClickListener: DialogInterface.OnClickListener? = null): AlertDialog {
        return showConfirmDialog(context, message, positiveClickListener, null, negativeClickListener, null)
    }

    /**
     * method, used to show confirm dialog with passed message string
     *
     * @param context
     * @param message
     * @param positiveClickListener
     */
    fun showConfirmDialog(context: Context, message: String,
                          positiveClickListener: DialogInterface.OnClickListener, positiveButtonText: String?,
                          negativeClickListener: DialogInterface.OnClickListener?, negativeButtonText: String?): AlertDialog {
        var positiveButtonText = positiveButtonText
        var negativeButtonText = negativeButtonText
        // create the dialog builder & set message
        val dialogBuilder = AlertDialog.Builder(context, R.style.MyDialog)
        dialogBuilder.setMessage(message)

        // prepare positive & negative buttons texts
        if (positiveButtonText == null) {
            positiveButtonText = context.getString(R.string.yes)
        }
        if (negativeButtonText == null) {
            negativeButtonText = context.getString(R.string.no)
        }

        // add positive click listener
        dialogBuilder.setPositiveButton(positiveButtonText, positiveClickListener)

        // check negative click listener
        if (negativeClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setNegativeButton(negativeButtonText, negativeClickListener)
        } else {
            // null
            // add new click listener to dismiss the dialog
            dialogBuilder.setNegativeButton(negativeButtonText) { dialog, which -> dialog.dismiss() }
        }
        // create and show the dialog
        val dialog = dialogBuilder.create()
        dialog.show()
        customizeDialogMsgTextView(dialog)

        return dialog
    }

    /**
     * method, used to show progress dialog with passed message res id
     *
     * @param context
     * @param messageResId
     * @param cancelable
     * @return
     */
    @JvmOverloads
    fun showProgressDialog(context: Context, messageResId: Int, cancelable: Boolean = false): android.app.AlertDialog? {
        return showProgressDialog(context, context.getString(messageResId), cancelable)
    }

    /**
     * method, used to show progress dialog with passed message string
     *
     * @param context
     * @param message
     * @param cancelable
     * @return the dialog
     */
    @JvmOverloads
    fun showProgressDialog(context: Context, message: String, cancelable: Boolean = false): android.app.AlertDialog? {
        val dialog = SpotsDialog.Builder().setContext(context).build()
        dialog.setMessage(message)
        dialog.setCanceledOnTouchOutside(cancelable)
        dialog.setCancelable(cancelable)
        dialog.show()
        customizeDialogMsgTextView(dialog)
        return dialog
    }

    /**
     * method, used to customize the message textview of a dialog
     *
     * @param dialog
     */
    private fun customizeDialogMsgTextView(dialog: Dialog) {
        /*******************************
         * No customizations need here,
         * Do nothing
         *
         * try {
         * Context context = dialog.getContext();
         * TextView textView = (TextView) dialog.findViewById(android.R.id.message);
         * textView.setTypeface(Typeface.createFromAsset(context.getAssets(), "app_font.ttf"));
         * textView.setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
         * context.getResources().getDimension(R.dimen.alert_dialog_text_spacing),
         * context.getResources().getDisplayMetrics()), 1.0f);
         * } catch (Exception e) {
         * e.printStackTrace();
         * }
         *
         */
    }

    /**
     * method, used to show list dialog with items array res id
     *
     * @param context
     * @param itemsResId
     * @param itemClickListener
     * @return
     */
    fun showListDialog(context: Context, itemsResId: Int, itemClickListener: DialogInterface.OnClickListener): AlertDialog {
        return showListDialog(context, null, itemsResId, itemClickListener)
    }

    /**
     * method, used to show list dialog with items array res id and with passed title
     *
     * @param context
     * @param title
     * @param itemsResId
     * @param itemClickListener
     * @return
     */
    fun showListDialog(context: Context, title: String?, itemsResId: Int, itemClickListener: DialogInterface.OnClickListener): AlertDialog {
        val items = context.resources.getStringArray(itemsResId)
        return showListDialog(context, title, items, itemClickListener)
    }

    /**
     * method, used to show list dialog with string items array
     *
     * @param context
     * @param items
     * @param itemClickListener
     * @return
     */
    fun showListDialog(context: Context, items: Array<String>, itemClickListener: DialogInterface.OnClickListener): AlertDialog {
        return showListDialog(context, null, items, itemClickListener)
    }

    /**
     * method, used to show list dialog with string items array and with passed title
     *
     * @param context
     * @param title
     * @param items
     * @param itemClickListener
     * @return
     */
    fun showListDialog(context: Context, title: String?, items: Array<String>, itemClickListener: DialogInterface.OnClickListener?): AlertDialog {
        // create the dialog builder
        val builder = AlertDialog.Builder(context, R.style.MyDialog)

        // set title if possible
        if (title != null) {
            builder.setTitle(title)
        }

        // set items and items click listener
        if (itemClickListener != null) {
            builder.setItems(items, itemClickListener)
        } else {
            builder.setItems(items) { dialog, which -> }
        }

        // create the dialog and show it
        val dialog = builder.create()
        dialog.show()

        return dialog
    }
}