package com.advisoryme.ibest.bus.utils

import android.app.*
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Typeface
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.UnderlineSpan
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.advisoryme.ibest.bus.R
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by Shamyyoun on 18/12/15.
 * A class, with general purpose utility methods (useful for many projects).
 */
object Utils {
    const val DEBUGGABLE = true
    const val PACKAGE_FACEBOOK = "com.facebook.katana"
    const val PACKAGE_FACEBOOK_MESSENGER = "com.facebook.orca"
    const val PACKAGE_GOOGLE_PLUS = "com.google.android.apps.plus"
    const val PACKAGE_WHATSAPP = "com.whatsapp"
    const val PACKAGE_PLAY_STORE = "com.android.vending"
    const val REGEX_MOBILE_NUMBER = "^(\\+\\d{1,3}[- ]?)?\\d{10,15}$"
    const val REGEX_EGYPTIAN_MOBILE_NUMBER = "^01[0-2|5]{1}[0-9]{8}"
    const val REGEX_EGYPTIAN_VODAFONE_MOBILE_NUMBER = "^010{1}[0-9]{8}"
    const val KEY_APP_VERSION_CODE = "app_version_code_key"
    const val STRONG_PASSWORD_REGEX = "^(?=.*[A-Za-z])(?=.*[0-9]).{8,}$"
    const val FACEBOOK_SHARER_URL = "https://www.facebook.com/sharer/sharer.php?u="

    /**
     * get the hash key
     */
    fun getHashKey(context: Context): String? {
        var hashKey: String? = null
        try {
            val info = context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                hashKey = Base64.encodeToString(md.digest(), Base64.DEFAULT)
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
            logE("Failed to get hash key")
        } catch (ignored: NoSuchAlgorithmException) {
            logE("Failed to get hash key")
        }

        return hashKey
    }


    /**
     * Validate email address.
     *
     * @param email the email to validate
     * @return true if valid email or false.
     */
    fun isValidEmail(email: CharSequence): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    /**
     * Get a shared preferences file named AppConst.AppConst.SHARED_PREFS_NAME, keys added to it must be unique
     *
     * @param ctx
     * @return the shared preferences
     */
    fun getSharedPreferences(ctx: Context): SharedPreferences {
        return ctx.getSharedPreferences(Const.PREFS_NAME, Context.MODE_PRIVATE)
    }

    fun cacheBoolean(ctx: Context, k: String, v: Boolean?) {
        val prefs = getSharedPreferences(ctx)
        prefs.edit().putBoolean(k, v!!).apply()
    }

    fun getCachedBoolean(ctx: Context, k: String, defaultValue: Boolean?): Boolean {
        val prefs = getSharedPreferences(ctx)
        return prefs.getBoolean(k, defaultValue!!)
    }

    fun cacheString(ctx: Context, k: String, v: String) {
        val prefs = getSharedPreferences(ctx)
        prefs.edit().putString(k, v).apply()
    }

    fun getCachedString(ctx: Context, k: String, defaultValue: String): String? {
        val prefs = getSharedPreferences(ctx)
        return prefs.getString(k, defaultValue)
    }

    fun cacheInt(ctx: Context, k: String, v: Int) {
        val prefs = getSharedPreferences(ctx)
        prefs.edit().putInt(k, v).apply()
    }

    fun getCachedInt(ctx: Context, k: String, defaultValue: Int): Int {
        val prefs = getSharedPreferences(ctx)
        return prefs.getInt(k, defaultValue)
    }

    fun clearCachedKey(context: Context, key: String) {
        getSharedPreferences(context).edit().remove(key).apply()
    }

    /**
     * Checks the device has a connection (not necessarily connected to the internet)
     *
     * @param ctx
     * @return
     */
    fun hasConnection(ctx: Context): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        if (wifiNetwork != null && wifiNetwork.isConnected) {
            return true
        }
        val mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (mobileNetwork != null && mobileNetwork.isConnected) {
            return true
        }
        val activeNetwork = cm.activeNetworkInfo
        return if (activeNetwork != null && activeNetwork.isConnected) {
            true
        } else false
    }

    /**
     * Gets a formatted % percent from numerator/denominator values.
     *
     * @param numerator
     * @param denominator
     * @param locale      the locale of the returned string.
     * @return the formatted percent.
     */
    fun getPercent(numerator: Int, denominator: Int, locale: Locale): String {
        //http://docs.oracle.com/javase/tutorial/i18n/format/decimalFormat.html
        val nf = NumberFormat.getNumberInstance(locale)  //Locale.US, .....
        val df = nf as DecimalFormat
        df.applyPattern("###.#")
        if (denominator == 0) {
            return df.format(0) + "%"
        }
        val percent = numerator / denominator.toFloat() * 100
        return df.format(percent.toDouble()) + "%"
    }


    /**
     * hide keyboard in edit text field
     */
    fun hideKeyboard(view: View) {
        val inputManager = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /**
     * hide keyboard in activity
     */
    fun hideKeyboard(activity: Activity) {
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    /**
     * hide keyboard in activity
     */
    fun hideKeyboard(dialog: Dialog) {
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    /**
     * hide keyboard in fragment
     */
    fun hideKeyboard(fragment: Fragment) {
        fragment.activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    /**
     * show key board in edit text field
     */
    fun showKeyboard(activity: Activity, et: EditText) {
        val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.showSoftInputFromInputMethod(et.windowToken, 0)
    }


    fun showShortToast(context: Context, text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }


    fun showShortToast(context: Context, textID: Int) {
        Toast.makeText(context, textID, Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(context: Context, text: String) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    fun showLongToast(context: Context, textID: Int) {
        Toast.makeText(context, textID, Toast.LENGTH_LONG).show()
    }

    fun logE(msg: String?) {
        if (DEBUGGABLE) Log.e(Const.LOG_TAG, msg ?: "null")
    }


    /**
     * Executes the given AsyncTask Efficiently.
     *
     * @param task the task to execute.
     */
    fun executeAsyncTask(task: AsyncTask<Void, Void, Void>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        } else {
            task.execute()
        }
    }

    /**
     * Remove '-' and other characters from mobile numbers and replace any + with 00
     *
     * @param oldNumber
     * @return the enhanced number
     */
    fun enhanceMobileNumber(oldNumber: String): String {
        return oldNumber.replace("[()\\s-]".toRegex(), "").replace("+", "00")
    }

    /**
     * Checks to see if EditText contains whitespace or no text
     *
     * @param et the EditText
     * @return true if EditText contains whitespace or no text otherwise false
     */
    fun isEmpty(et: EditText): Boolean {
        return TextUtils.isEmpty(et.text.toString().trim { it <= ' ' })
    }

    /**
     * Checks to see if text is null or contains whitespace or no content
     *
     * @param charSequence
     * @return true if text contains whitespace or no content otherwise false
     */
    fun isEmpty(charSequence: CharSequence?): Boolean {
        return if (charSequence == null) true else TextUtils.isEmpty(charSequence.toString().trim { it <= ' ' })
    }

    /**
     * Get the EditText text trimmed
     *
     * @param et
     * @return the EditText text trimmed
     */
    fun getText(et: EditText): String {
        return et.text.toString().trim { it <= ' ' }
    }

    /**
     * Get the Editable text trimmed
     *
     * @param editable
     * @return the text trimmed
     */
    fun getText(editable: Editable): String {
        return editable.toString().trim { it <= ' ' }
    }

    /**
     * Get the EditText text as integer
     *
     * @param et
     * @return the EditText text as integer
     */
    fun getInt(et: EditText): Int {
        val text = getText(et)
        return convertToInt(text)
    }

    /**
     * converts the given timestamp to  readable date.
     *
     * @param timeStamp
     * @return
     */
    fun timeStampToString(timeStamp: Long, locale: Locale): String {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", locale)
        return dateFormat.format(Date(timeStamp))
    }


    /**
     * Gets the app version code.
     *
     * @param context
     * @return the version code.
     */
    fun getAppVersionCode(context: Context): Int {
        try {
            val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            return packageInfo.versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            // should never happen
            throw RuntimeException("Could not get package name: $e")
        }

    }

    fun cacheAppVersionCode(context: Context) {
        cacheInt(context, KEY_APP_VERSION_CODE, getAppVersionCode(context))
    }


    fun getCachedAppVersionCode(context: Context): Int {
        return getCachedInt(context, KEY_APP_VERSION_CODE, Integer.MIN_VALUE)
    }

    /**
     * Get the application version name
     *
     * @param context
     * @return The app version name
     */
    fun getAppVersionName(context: Context): String {
        try {
            val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            return packageInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            // should never happen
            throw RuntimeException("Could not get package name: $e")
        }

    }

    /**
     * Gets the current app locale language like: ar, en, ....
     *
     * @return the current app locale language like: ar, en, ....
     */
    val appLanguage: String
        get() = Locale.getDefault().language.toLowerCase().substring(0, 2)

    /**
     * Set the app language
     *
     * @param ctx  the application context
     * @param lang the language as ar, en, .....
     */
    fun changeAppLocale(ctx: Context, lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        ctx.resources.updateConfiguration(config, ctx.resources.displayMetrics)
    }

    /**
     * method, used to format a double number as string with maximum 1 number after the point
     *
     * @param number
     * @param locale
     * @return the formatted double as string
     */
    @JvmOverloads
    fun formatDouble(number: Double, locale: Locale? = null): String {
        var locale = locale
        // check locale
        if (locale == null) {
            locale = Locale.ENGLISH // english locale by default
        }

        return if (number == number.toLong().toDouble()) {
            String.format(locale!!, "%d", number.toLong())
        } else {
            String.format(locale!!, "%.1f", number)
        }
    }

    /**
     * method, used to format a double number to the highest integer number
     *
     * @param number
     * @return
     */
    fun formatDoubleToHighestInteger(number: Double): String {
        var number = number
        number = Math.ceil(number)
        return formatDouble(number, null)
    }

    /**
     * method, used to format a double number to the highest integer number
     *
     * @param number
     * @param locale
     * @return
     */
    fun formatDoubleToHighestInteger(number: Double, locale: Locale): String {
        var number = number
        number = Math.ceil(number)
        return formatDouble(number, locale)
    }

    /**
     * Checks if a specified service is running or not.
     *
     * @param ctx          the context
     * @param serviceClass the class of the service
     * @return true if the service is running otherwise false
     */
    fun isServiceRunning(ctx: Context, serviceClass: Class<*>): Boolean {
        val manager = ctx.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    /**
     * method, used to format the url to prevent app from crash when open browser intent
     *
     * @param url
     * @return the formatted url
     */
    fun formatUrl(url: String): String {
        var url = url
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://$url"
        }
        return url
    }

    /**
     * method, used to check if string is null or empty
     *
     * @param str to check
     * @return boolean true if null or empty
     */
    fun isNullOrEmpty(str: String?): Boolean {
        return str == null || str.isEmpty()
    }

    /**
     * method, used to check if list is null or empty
     *
     * @param list to check
     * @return boolean true if null or empty
     */
    fun isNullOrEmpty(list: List<*>?): Boolean {
        return list == null || list.isEmpty()
    }

    /**
     * method, used to check if arr is null or empty
     *
     * @param arr to check
     * @return boolean true if null or empty
     */
    fun isNullOrEmpty(arr: Array<Any>?): Boolean {
        return arr == null || arr.size == 0
    }


    /**
     * method, used to prepare the url and open it in the browser
     *
     * @param context
     * @param url
     */
    fun openBrowser(context: Context, url: String) {
        var url = url
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://$url"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }

    /**
     * method, used to check if app is installed in the device or not
     *
     * @param context
     * @param appPackageName
     * @return
     */
    fun isAppInstalledAndEnabled(context: Context, appPackageName: String): Boolean {
        try {
            val pm = context.packageManager
            val info = pm.getPackageInfo(appPackageName, PackageManager.GET_ACTIVITIES)
            val installed = true
            val enabled = info.applicationInfo.enabled

            return installed && enabled
        } catch (e: PackageManager.NameNotFoundException) {
            return false
        }

    }

    /**
     * method, used to share text to specific app
     *
     * @param context
     * @param appPackageName
     * @param text
     * @return
     */
    fun shareTextToApp(context: Context, appPackageName: String, text: String): Boolean {
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.setPackage(appPackageName)
            intent.putExtra(Intent.EXTRA_TEXT, text)
            context.startActivity(intent)
            return true
        } catch (e: Exception) {
            // app is not installed
            return false
        }

    }

    /**
     * method, used to convert string number to double number
     *
     * @param number
     * @return
     */
    fun convertToDouble(number: String): Double {
        try {
            return java.lang.Double.parseDouble(number)
        } catch (e: Exception) {
            e.printStackTrace()
            return 0.0
        }

    }

    /**
     * method, used to convert string number to int number
     *
     * @param number
     * @return
     */
    fun convertToInt(number: String): Int {
        try {
            return Integer.parseInt(number)
        } catch (e: Exception) {
            e.printStackTrace()
            return 0
        }

    }

    /**
     * method, used to concatenate array of strings with the passed divider
     *
     * @param divider
     * @param strings
     * @return
     */
    fun getFullString(divider: String, vararg strings: String): String {
        var finalString = ""
        for (str in strings) {
            if (!isNullOrEmpty(str)) {
                if (finalString.isEmpty()) {
                    finalString += str
                } else {
                    finalString += divider + str
                }
            }
        }

        return finalString
    }

    /**
     * method, used to match the regex on the passed text and return true or false
     *
     * @param regex
     * @param text
     * @return
     */
    fun matchRegex(regex: String, text: String): Boolean {
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(text)

        return matcher.matches()
    }

    /**
     * method, used to override a font in all the app
     *
     * @param context
     * @param staticFontName
     * @param fontAssetName
     */
    fun overrideFont(context: Context, staticFontName: String, fontAssetName: String) {
        val regular = Typeface.createFromAsset(context.assets, fontAssetName)
        try {
            val staticField = Typeface::class.java.getDeclaredField(staticFontName)
            staticField.isAccessible = true
            staticField.set(null, regular)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * method, used to open the phone intent using the passed phone number
     *
     * @param context
     * @param phone
     */
    fun openPhoneIntent(context: Context, phone: String) {
        if (Utils.isNullOrEmpty(phone)) {
            return
        }

        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phone")
        context.startActivity(intent)
    }

    /**
     * method, used to open the email intent using the passed email address
     *
     * @param context
     * @param emailAddress
     */
    fun openEmailIntent(context: Context, emailAddress: String) {
        if (Utils.isNullOrEmpty(emailAddress)) {
            return
        }

        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailAddress))
        intent.type = "message/rfc822"
        context.startActivity(intent)
    }

    /**
     * method, used to open the map intent with passed params
     *
     * @param context
     * @param lat
     * @param lng
     */
    fun openMapIntent(context: Context, lat: Double, lng: Double) {
        openMapIntent(context, null, lat, lng)
    }

    /**
     * method, used to open the map intent with passed params
     *
     * @param context
     * @param title
     * @param lat
     * @param lng
     */
    fun openMapIntent(context: Context, title: String?, lat: Double, lng: Double) {
        var geoUri = "http://maps.google.com/maps?q=loc:$lat,$lng"
        if (!isNullOrEmpty(title)) {
            geoUri += " ($title)"
        }
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
        context.startActivity(intent)
    }

    /**
     * method, used to set the text underlined in the text view
     *
     * @param textView
     * @param text
     */
    fun setUnderlined(textView: TextView, text: String) {
        val spannable = SpannableString(text)
        spannable.setSpan(UnderlineSpan(), 0, text.length, 0)
        textView.text = spannable
    }

    /**
     * method, used to trim string if not null or return null
     *
     * @param str
     * @return
     */
    fun trim(str: String?): String? {
        return str?.trim { it <= ' ' }
    }

    /**
     * method, used to reverse array of objects and return it
     *
     * @param arr
     * @return
     */
    fun reverseArray(arr: Array<Any>): Array<Any> {
        for (i in 0 until arr.size / 2) {
            val tmpObject = arr[i]
            arr[i] = arr[arr.size - i - 1]
            arr[arr.size - i - 1] = tmpObject
        }

        return arr
    }

    /**
     * method, used to open the app page in play store
     *
     * @param context
     */
    fun getAppPlayStorePageIntent(context: Context): Intent {
        val packageName = context.packageName
        val url: String
        if (isAppInstalledAndEnabled(context, PACKAGE_PLAY_STORE)) {
            url = "market://details?id=$packageName"
        } else {
            url = "https://play.google.com/store/apps/details?id=$packageName"
        }
        return Intent(Intent.ACTION_VIEW, Uri.parse(url))
    }

    /**
     * method, used to open the app page in play store
     *
     * @param context
     */
    fun openAppInPlayStore(context: Context) {
        context.startActivity(getAppPlayStorePageIntent(context))
    }

    /**
     * method, used to get facebook profile url
     *
     * @param userId
     * @return
     */
    fun getFacebookProfileUrl(userId: String): String {
        return "https://www.facebook.com/$userId"
    }

    /**
     * method, check if the url is valid or not
     *
     * @param url
     * @return
     */
    fun isValidUrl(url: CharSequence): Boolean {
        return !TextUtils.isEmpty(url) && Patterns.WEB_URL.matcher(url).matches()
    }

    /**
     * method, used to convert a boolean to int
     *
     * @param b
     * @return
     */
    fun convertToInt(b: Boolean): Int {
        return if (b) 1 else 0
    }

    /**
     * method used to check if the recycler linear layout manager has reached the end or not
     *
     * @param layoutManager
     * @return
     */
    fun isReachedEndOfRecycler(layoutManager: LinearLayoutManager): Boolean {
        // get counts
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()

        // check total item count
        return if (totalItemCount == 0) {
            true
        } else {
            visibleItemCount + pastVisibleItems >= totalItemCount
        }
    }

    /**
     * method, used to check if reached end of the recycler or not
     *
     * @param recyclerView
     * @return
     */
    fun isReachedEndOfRecycler(recyclerView: RecyclerView): Boolean {
        return !recyclerView.canScrollVertically(1)
    }

    /**
     * method, used to format the url (remove spaces for example)
     *
     * @param url
     * @return
     */
    fun getFormattedUrl(url: String): String {
        var url = url
        url = url.replace(" ".toRegex(), "+")
        return url
    }

    /**
     * method, used to convert int value to boolean
     *
     * @param value
     * @return
     */
    fun convertToBoolean(value: Int): Boolean {
        return value != 0
    }

    /**
     * method, used to convert string value to boolean
     *
     * @param value
     * @return
     */
    fun convertToBoolean(value: String): Boolean {
        return "true".equals(value, ignoreCase = true)
    }

    /**
     * method, used to play the default notification sound
     */
    fun playNotificationSound(context: Context) {
        try {
            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val ringtone = RingtoneManager.getRingtone(context, notification)
            ringtone.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * method, used to copy text to the clipboard
     *
     * @param context
     * @param text
     */
    fun copyToClipboard(context: Context, text: String) {
        val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("", text)
        clipboard.primaryClip = clip
    }

    /**
     * method, used to encode a query param to be sent in the url
     *
     * @param param
     * @return
     */
    fun encodeQueryParam(param: String): String {
        var param = param
        try {
            param = URLEncoder.encode(param, "utf-8")
        } catch (e: UnsupportedEncodingException) {
        }

        return param
    }

    /**
     * method, used to add query param to the url
     *
     * @param url
     * @param key
     * @param value
     * @return
     */
    fun addQueryParam(url: String, key: String, value: Any): String? {
        var url = url
        var value = value
        // check the url
        if (isNullOrEmpty(url)) {
            return null
        }

        // encode the param value
        value = encodeQueryParam(value.toString())

        // check if first param
        if (url.contains("?")) {
            // not first param
            url += "&$key=$value"
        } else {
            // the first param
            url += "?$key=$value"
        }

        // then return the url
        return url
    }

    /**
     * method, used to get the app url in play store
     *
     * @param context
     * @return
     */
    fun getPlayStoreAppUrl(context: Context): String {
        var url = "https://play.google.com/store/apps/details?id="
        url += context.packageName

        return url
    }

    /**
     * method, used to share text
     *
     * @param context
     * @param text
     * @return
     */
    fun shareText(context: Context, text: String): Boolean {
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, text)
            context.startActivity(intent)
            return true
        } catch (e: Exception) {
            return false
        }

    }

    /**
     * Validate phone number.
     *
     * @param phoneNumber the phone number to validate
     * @return true if valid phone number or false.
     */
    fun isValidPhoneNumber(phoneNumber: String): Boolean {
        return matchRegex(REGEX_MOBILE_NUMBER, phoneNumber)
    }

    /**
     * Validate egyptian mobile number.
     *
     * @param mobileNumber the mobile number to validate
     * @return true if valid egyptian mobile number or false.
     */
    fun isValidEgyptianMobileNumber(mobileNumber: String): Boolean {
        var mobileNumber = mobileNumber
        // format the mobile number
        mobileNumber = formatEgyptianMobileNumber(mobileNumber)

        // match the pattern
        return matchRegex(REGEX_EGYPTIAN_MOBILE_NUMBER, mobileNumber)
    }

    /**
     * Validate egyptian vodafone mobile number.
     *
     * @param mobileNumber the mobile number to validate
     * @return true if valid egyptian vodafone mobile number or false.
     */
    fun isValidVodafoneMobileNumber(mobileNumber: String): Boolean {
        var mobileNumber = mobileNumber
        // format the mobile number
        mobileNumber = formatEgyptianMobileNumber(mobileNumber)

        // match the pattern
        return matchRegex(REGEX_EGYPTIAN_VODAFONE_MOBILE_NUMBER, mobileNumber)
    }

    /**
     * method, used to format an egyptian mobile number by removing white spaces and +2 at the first
     *
     * @param mobileNumber
     * @return
     */
    fun formatEgyptianMobileNumber(mobileNumber: CharSequence): String {
        var mobileNumberStr = mobileNumber.toString()
        mobileNumberStr = mobileNumberStr.replace(" ".toRegex(), "")
        if (mobileNumberStr.startsWith("2")) {
            mobileNumberStr = mobileNumberStr.replace("2", "")
        }
        if (mobileNumberStr.startsWith("+2")) {
            mobileNumberStr = mobileNumberStr.replace("+2", "")
        }
        if (mobileNumberStr.startsWith("002")) {
            mobileNumberStr = mobileNumberStr.replace("002", "")
        }

        return mobileNumberStr
    }

    /**
     * method, used to return non numbers values from a text
     *
     * @param text
     * @return
     */
    fun getWordsFromText(text: String?): String {
        return text?.replace("[0-9]".toRegex(), "") ?: ""
    }

    /**
     * method, used to return numbers only from a text
     *
     * @param text
     * @return
     */
    fun getNumbersFromText(text: String?): String {
        return text?.replace("[^0-9]".toRegex(), "") ?: ""
    }

    /**
     * method, used to read file content in assets folder
     *
     * @param context
     * @param fileName
     * @return
     */
    fun readFileFromAssets(context: Context, fileName: String): String? {
        var content: String? = null
        var reader: BufferedReader? = null
        try {
            reader = BufferedReader(
                    InputStreamReader(context.assets.open(fileName)))

            content = ""
            var line = reader.readLine()
            while (line != null) {
                content += line
            }
        } catch (e: IOException) {
            if (DEBUGGABLE) e.printStackTrace()
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: IOException) {
                    if (DEBUGGABLE) e.printStackTrace()
                }

            }
        }

        return content
    }

    /**
     * method, used to get query value of a url
     *
     * @param url
     * @param key
     * @return
     */
    fun getQueryValue(url: String, key: String): String? {
        try {
            val uri = Uri.parse(url)
            return uri.getQueryParameter(key)
        } catch (e: Exception) {
            return null
        }

    }

    /**
     * method, used to open profile in facebook app if possible
     *
     * @param context
     * @param profileUrl
     */
    fun openFacebookProfile(context: Context, profileUrl: String) {
        var profileUrl = profileUrl
        // open in the facebook app if possible
        try {
            val packageManager = context.packageManager
            val versionCode = packageManager.getPackageInfo(PACKAGE_FACEBOOK, 0).versionCode
            if (versionCode >= 3002850) {
                profileUrl = "fb://facewebmodal/f?href=$profileUrl"
            } else {
                profileUrl = "fb://profile/$profileUrl"
            }

            // open the intent
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(profileUrl))
            context.startActivity(intent)
        } catch (e: Exception) {
            // open profile in browser
            openBrowser(context, profileUrl)
        }

    }

    /**
     * method, used to return facebook profile picture url
     *
     * @param facebookUserId
     * @return
     */
    fun getFacebookProfilePictureUrl(facebookUserId: String): String {
        return "https://graph.facebook.com/$facebookUserId/picture?type=large"
    }

    /**
     * method, used to check if the password is strong or not
     *
     * @param password
     * @return
     */
    fun isStrongPassword(password: String): Boolean {
        return matchRegex(STRONG_PASSWORD_REGEX, password)
    }

    /**
     * method, used to share url to the facebook sharer url
     *
     * @param context
     * @param url
     */
    fun shareUrlToFacebookSharer(context: Context, url: String) {
        openBrowser(context, FACEBOOK_SHARER_URL + url)
    }

    fun showNotification(context: Context, message: String, id: Int = Calendar.getInstance().timeInMillis.toInt(),
                         title: String = context.getString(R.string.app_name), intent: Intent = Intent()) {
        // create the pending intent
        val pendingIntent = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_ONE_SHOT)

        // create and customize the notification
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

        // change notification style
        val style = NotificationCompat.BigTextStyle()
        style.bigText(message)
        notificationBuilder.setStyle(style)

        // show the notification
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        synchronized(notificationManager) {
            notificationManager.notify(id, notificationBuilder.build())
        }
    }
}
