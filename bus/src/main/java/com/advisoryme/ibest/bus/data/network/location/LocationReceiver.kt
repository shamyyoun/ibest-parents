package com.advisoryme.ibest.bus.data.network.location

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import com.advisoryme.ibest.bus.R
import com.advisoryme.ibest.bus.data.models.IBestLocation
import com.advisoryme.ibest.bus.utils.Const
import com.advisoryme.ibest.bus.utils.InjectionUtils
import com.advisoryme.ibest.bus.utils.LocationUtils
import com.advisoryme.ibest.bus.utils.Utils
import java.util.*

class LocationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Utils.logE("location service is running...")

        // validate the context
        if (context == null) {
            return
        }

        // create the location repository
        val locationRepository = InjectionUtils.provideLocationRepository(context)

        // check if it's came from a boot, so schedule the repeated task
        if (intent?.action == "android.intent.action.BOOT_COMPLETED") {
            locationRepository.startLocationUpdates()
        }

        // check permissions
        if (!LocationUtils.checkLocationPermissions(context)) {
            // show notification
            val message = context.getString(R.string.location_permissions_are_not_granted_grant_them)
            Utils.showNotification(context, message, Const.NOTI_PERMISSION_NOT_GRANTED)

            // stop the location updates
            locationRepository.stopLocationUpdates()

            return
        }

        // check gps
        if (!LocationUtils.checkGps(context)) {
            // show notification
            val message = context.getString(R.string.gps_is_not_enabled_enable)
            Utils.showNotification(context, message, Const.NOTI_GPS_IS_NOT_ENABLED)

            // stop the location updates
            locationRepository.stopLocationUpdates()

            return
        }

        // get the location and check it
        val location = LocationUtils.getLastKnownLocation(context)
        if (location != null) {
            // prepare values
            val timestamp = Calendar.getInstance().timeInMillis
            var speed = 0
            var distance = 0f

            // get last saved location
            val lastLocation = locationRepository.getLastLocation()

            // check it
            if (lastLocation != null) {
                // calculate distance
                val lastLocationAndroid = Location("")
                lastLocationAndroid.latitude = lastLocation.lat
                lastLocationAndroid.longitude = lastLocation.lng
                distance = location.distanceTo(lastLocationAndroid)

                // calculate speed
                val timeInSeconds = (timestamp - lastLocation.timestamp) / 1000
                speed = (distance / timeInSeconds).toInt()
            }

            // update it
            locationRepository.updateLocation(location.latitude, location.longitude, speed, distance, timestamp).observeForever {
                if (it != null && it.result == true) {
                    // success, save the location
                    val iBestLocation = IBestLocation(location.latitude, location.longitude, timestamp)
                    locationRepository.updateLastLocation(iBestLocation)
                } else {
                    // show message
                    val message = it?.message
                            ?: context.getString(R.string.failed_updating_your_location_check_internet)
                    Utils.showLongToast(context, message)
                }
            }

        } else {
            Utils.showLongToast(context, R.string.failed_getting_your_location_check_settings)
        }
    }
}