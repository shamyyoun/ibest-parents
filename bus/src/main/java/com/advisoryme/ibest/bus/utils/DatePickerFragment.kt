package com.advisoryme.ibest.bus.utils

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window
import com.advisoryme.ibest.bus.R
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 */
class DatePickerFragment : DialogFragment() {
    var datePickerListener: DatePickerDialog.OnDateSetListener? = null
    private var date: Calendar? = null
    private var minDate: Calendar? = null
    private var maxDate: Calendar? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (date == null) {
            // Use the current date as the default date in the picker
            date = Calendar.getInstance()
        }

        val year = date!!.get(Calendar.YEAR)
        val month = date!!.get(Calendar.MONTH)
        val day = date!!.get(Calendar.DAY_OF_MONTH)

        // create DatePickerDialog instance and customize it
        val datePickerDialog = DatePickerDialog(activity!!, R.style.MyDialog,
                datePickerListener, year, month, day)
        datePickerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        // set min & max if possible
        if (minDate != null) {
            datePickerDialog.datePicker.minDate = minDate!!.timeInMillis
        }
        if (maxDate != null) {
            datePickerDialog.datePicker.maxDate = maxDate!!.timeInMillis
        }

        return datePickerDialog
    }

    fun setDate(calendar: Calendar) {
        this.date = calendar
    }

    fun setDate(date: String?, dateFormat: String) {
        if (date != null) {
            try {
                this.date = Calendar.getInstance()
                val sdf = SimpleDateFormat(dateFormat, Locale.getDefault())
                this.date!!.time = sdf.parse(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    fun setMinDate(minDate: Calendar) {
        this.minDate = minDate
    }

    fun setMinDateToToday() {
        this.minDate = Calendar.getInstance()
    }

    fun setMinDate(minDateStr: String, dateFormat: String) {
        minDate = DateUtils.convertToCalendar(minDateStr, dateFormat)
    }

    fun setMaxDate(maxDate: Calendar) {
        this.maxDate = maxDate
    }

    fun setMaxDate(maxDateStr: String, dateFormat: String) {
        maxDate = DateUtils.convertToCalendar(maxDateStr, dateFormat)
    }
}