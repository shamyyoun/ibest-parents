package com.advisoryme.ibest.bus.data.network.location

import com.advisoryme.ibest.bus.data.network.utils.ApiInjectionUtils
import com.advisoryme.ibest.bus.utils.InjectionUtils
import retrofit2.Call

class LocationApiImpl {
    private val locationApi: LocationApi

    init {
        val apiManagerImpl = InjectionUtils.provideApiManager()
        locationApi = apiManagerImpl.createApi(LocationApi::class.java)
    }

    fun updateLocation(lat: Double, lng: Double, speed: Int, distance: Float, timestamp: Long): Call<Void> {
        val body = ApiInjectionUtils.provideUpdateLocationBody(lat, lng, speed,
                distance, timestamp)
        return locationApi.updateLocation(body)

    }
}